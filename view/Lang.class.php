<?php

namespace core\view;

/**
 * Classe Util que fornece algumas funcionalidades para trabalhar 
 * com strings internacionalizadas no sistema. 
 * 
 * Recomenda-se utilizar um lib própria para essa tarefa no entanto
 * para sistemas pequenos como o RevisãoOnline essa classe da conta do 
 * recado
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @package core.view
 */
class Lang
{

    public static $lang = '';
    private static $palavras = [];

    /**
     * Carrega as Strings salvas na pasta de intenacionalização 
     * localizado em view/templates/i18n/[lang]/strings.ini
     *
     */
    public static function load()
    {
        $file = TEMPLATES . '/i18n/' . strtolower(self::$lang) . '/STRINGS.ini';
        if (file_exists($file)) {
            self::$palavras = parse_ini_file($file, true);
        } 
    }

    public static function getName(){
        if (strtolower(self::$lang) == 'pt'){
            return 'pt-br';
        }else{
            return strtolower(self::$lang);
        }
    }

    /**
     * Retorna a string na lingua se disponível senão retorna a própria string
     *
     * @param string $stringOriginal
     * @return mixed
     */
    public static function get($stringOriginal)
    {
        try {
            if(is_array($stringOriginal)){
                return $stringOriginal;
            }
            // Tentativa de conversão para string se for objeto
            $stringOriginal = (string) $stringOriginal;   
            $string = str_replace('!', '', $stringOriginal);
            if (isset(self::$palavras[$string])) {
                return str_replace(  $string, self::$palavras[$string], $stringOriginal);
            }
            return $stringOriginal;
        } catch (\Exception $e) {
            return $stringOriginal;
        }
     
    }

    /**
     * Adiciona a string no mapa de traduções
     *
     * @param string $key Chave para adicionar
     * @param mixed $value Chave para adicionar
     */
    public static function put($key, $value)
    {
        self::$palavras[$key] = $value;
    }

}
