<!DOCTYPE html>
<html lang="{$LANGUAGE}">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" >
        <title>{$TITLE} </title>
        <link rel="shortcut icon" href="{$FAVICON}">

        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <meta name="keywords" content="{$KEYWORDS}">
        <meta name="description" property="og:description" content="{$DESCRIPTION}">
        <meta name="title" property="og:title" content="{$TITLE}">
        
    {foreach $METATAGS as $name => $content}
        <meta name="{$name}" content="{$content}" >    
    {/foreach}    

    {*OpenGraph SECTION*}
    {$OPENGRAPH->process()}

    {if isset($CANONICAL)}
        <link rel="canonical" href="{$CANONICAL}" />       
    {/if}

    {foreach $CDN->getCSS() as $libcss}
        <link rel="stylesheet" type="text/css" href="{$libcss}">
    {/foreach}
        
    {foreach $filesCSS as $css}
        <link rel="stylesheet" type="text/css" href="{$css['src']}" {$css['extra']}>
    {/foreach}
