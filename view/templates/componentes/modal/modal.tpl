<div id="EnyModalTPL">
{literal}
    <div class="modal" id="{{data.id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{data.titulo}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <div class="areaBotoes"></div>
                </div>
            </div>
        </div>
    </div>
{/literal}
</div>
