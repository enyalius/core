<?php

use core\view\MensagemSistema;
use core\view\Lang;
use core\view\OpenGraph;


/**
 * Description of AbstractView
 *
 * Changelog
 * 1.0.0 - Features fundamentais
 * 2.0.0 - 
 *   -Movimentação do core client para o servidor enviando apenas os arquivos compactados para a pasta do cliente
 * 2.1.0 - Movimentado o templateEngine para a pasta view.
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 2.1.0
 * @package core.view
 */
abstract class AbstractView
{

    protected $templates = [];
    protected $filesJS = [];
    protected $modulesJS = [];
    protected $libJS = [];
    protected $filesCSS = [];
    protected $selfScript;
    protected $varsJS = '';
    protected $selfScriptPos;
    /**
     * Váriavel que define qual é a pagina corente para o template 
     * poder utilizar essa informação para links ativos e breadcumbs
     *
     * @var [string]
     */
    protected $page;

    /**
     * @var string linguagem do sistema padrão pt-br. 
     */
    protected $lang = 'pt-br';

    /**
     *
     * @var \core\view\CDNManager
     */
    protected $CDN;

    /**
     *
     * @var string título da página 
     */
    private $title;
    private $description;
    private $keywords;
    protected $metaTags = [];
    private $favicon = '/imagens/favicon.png';


    /**
     *
     * @var OpenGraph
     */
    public $og;



    private static $render = false;


    /**
     *
     * @var \core\view\MensagemSistema
     */
    private $mensagemSistema;

    /**
     *
     * @var \core\view\templateEngine\TemplateEngine
     */
    private $renderEngine;

    public function __construct()
    {
        $this->selfScript = '';
        $this->renderEngine = new core\view\templateEngine\TemplateEngine();
        $this->CDN = new core\view\CDNManager();
        $this->mensagemSystem();
        $this->CDN->build();
        $this->baseJs();
        $this->attValue('BASE_URL', BASE_URL);
        $this->addCSS('eny');
        $this->gtag();
        $this->og = new OpenGraph($this);
    }

    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Seta o título da página para o navegador e para o 
     * 
     * @param mixed $title 
     */
    public function setTitle($title)
    {
        $this->title = Lang::get($title);
        return $this;
    }


    /**
     * Método que verifica se tem alguma configuração de sistema que define a 
     * linguagem padrão
     * 
     */
    public function defineLanguage()
    {
        if (defined('LANG')) {
            $this->setLang(LANG);
        }
    }


    /**
     * 
     * @return string linguagem setada no sistema
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Método que seta a lingua que vai ser exibida o sistema. 
     * 
     * Esse método também carrega as strings disponiveis na pasta 
     *    - app/view/i18n/lang/STRINGS.ini
     * 
     * @param string $lang
     * @return $this
     */
    public function setLang($lang)
    {
        $this->lang = strtolower($lang);
        if (count($this->templates) > 0) {
            foreach ($this->templates as $chave => $tpl) {
                $this->templates[$chave] = $this->verifyTemplate(substr($tpl, 0, strlen($tpl) - 4));
            }
        }
        Lang::$lang = $lang;
        Lang::load();
        $this->attValueJS('LANG', $lang);
        return $this;
    }

    /**
     * Adiciona um template a tela.
     * 
     * OBS: extensão do template é inserida automaticamente
     * 
     * @param string $template caminho do template a partir da pasta de templates
     */
    public function addTemplate($template)
    {
        $this->templates[] = $this->verifyTemplate($template);
    }

    /**
     * Verifica que se existe um template na lingua setada no sistema senão retorna template padrão
     * 
     * @param string $template
     * @return string
     */
    protected function verifyTemplate($template)
    {
        $template = preg_replace('/\.tpl$/', '', $template);

        if (!empty($this->lang)) {
            if (is_file(TEMPLATES . $template . '_' . $this->lang . '.tpl')) {
                return $template . '_' . $this->lang . '.tpl';
            }
            if (is_file(TEMPLATES . 'i18n/' . $this->lang . '/' . $template . '.tpl')) {
                return TEMPLATES . 'i18n/' . $this->lang . '/' . $template . '.tpl';
            }
        }
        return $template . '.tpl';
    }

    /**
     *
     * @return core\view\CDNManager
     */
    public function CDN()
    {
        return $this->CDN;
    }

    /**
     * Método que permite adicionar um componente.
     *
     * @param Componente $c
     */
    public function addComponente(Componente $c)
    {
        $c->setView($this);
        $c->add();
    }

    /**
     * Adiciona um script na página através de PHP útil para scripts gerados automaticamente
     * 
     * @param string $script
     * @param boolean $posLibs
     */
    public function addSelfScript($script, $posLibs = false)
    {
        $nl = DEBUG ? PHP_EOL : '';
        if ($posLibs) {
            $this->selfScriptPos .= $script . $nl;
        } else {
            $this->selfScript .= $script . $nl;
        }
    }

    /**
     * Usar addCSS se for lib a mesma deve ser carregada pelo gerenciador do 
     * CDN do enyalius
     * 
     * @param string $css
     * @deprecated since version 2.0
     */
    public function addLibCSS($css)
    {
        $this->addCSS($css);
    }

    public function addLibJS($js, $path = null)
    {
        $js = preg_replace('/\.js$/', '', $js);

        if (is_null($path)) {
            $path = '/js/dist/';
        }
        $this->libJS[$js] = $path . $js .'.js';
    }

    /**
     * Método que permita adicionar um css por caminho absoluto. 
     * 
     * Útil para adicionar CDNs não padronizados, ou css gerado pelo sistema.
     * 
     * @param string $src
     * @param string $media Midia type accepted por exemplo print
     */
    public function addAbsoluteCSS($src, $media = '')
    {
        $add = !empty($media) ? 'media="' . $media . '"' : $media;
        $this->filesCSS[$src] = ['src' => $src, 'extra' => $add];
    }

    /**
     * Método que adiciona um recurso de css a view atual. 
     * 
     * @param string $css
     * @param [String $media] Midia type accepted por exemplo print
     */
    public function addCSS($css, $media = '')
    {
        $css = preg_replace('/\.css$/', '', $css);

        $src = "/css/{$css}.css";
        $this->addAbsoluteCSS($src, $media);
    }

    public function addJS($js)
    {
        $path = DEBUG ? '' : 'dist/';
        $js = preg_replace('/\.js$/', '', $js);
        $this->filesJS[$js] = "/js/{$path}{$js}.js";
    }

    public function addMJS($js)
    {
        $path = DEBUG ? '' : 'dist/';
        $this->modulesJS[$js] = $path . $js;
    }

    /**
     * Método que permita adicionar um js por caminho absoluto.
     *
     * Útil para adicionar CDNs não padronizados, ou libs não padrões pelo sistema.
     *
     * @param string $src
     */
    public function addAbsoluteModuleJS($src)
    {
        $js = preg_replace('/\.js$/', '', $src);
        $this->modulesJS[$src] =  $js . '.js';
    }

       /**
     * Método que permita adicionar um js por caminho absoluto.
     *
     * Útil para adicionar CDNs não padronizados, ou libs não padrões pelo sistema.
     *
     * @param string $src
     */
    public function addAbsoluteJS($src)
    {
        $js = preg_replace('/\.js$/', '', $src);

        $this->libJS[$src] = $js . '.js';
    }


    /**
     * Seta os valores das variaveis do Template a ser carregado pelo visualizador.
     *
     * @param string  $variavel = Nome da variavel que será setada
     * @param mixed  $valor = Valor para setar variável.     *
     */
    public function attValue($variavel, $valor)
    {
        return $this->renderEngine->assign($variavel, $valor);
    }

    /**
     * Cria uma variável em javaScript com o valor que recebe como argumento se
     * for um array ou um objeto transforma esse em um objeto JavaScript e atribui
     * para a váriavel definida como argumento 1.
     *
     * @param string  $variavel  Nome da variável que será setada
     * @param mixed  $valor  Valor para setar variável.
     */
    public function attValueJS($variavel, $valor)
    {
        if (is_array($valor) || is_object($valor)) {
            $valor = JSON::encode($valor);
        } else if (!is_numeric($valor)) {
            $valor = JSON::encode($valor);
        }
        $this->varsJS .= 'var ' . $variavel . ' = ' . $valor . ';' . PHP_EOL;
    }

    /**
     * Método que seleciona todos os templates scripts e css e gera a tela.
     * 
     */
    public function render()
    {
        self::$render = true;
        $this->attDefault();
        $this->show(CORE . 'view/templates/generic/header.tpl');
        $this->mostrarTemplatesNaTela();
        $this->show(CORE . 'view/templates/generic/footer.tpl');
    }

    /**
     * Método que renderiza a página em formato ajax e não em formato html padrão 
     * adicionando todos os templates default scripts e css.
     */
    public function renderAjax()
    {
        $this->setRenderizado();
        $this->mostrarTemplatesNaTela();
    }

    /**
     * Método que remove todos os templates colocados por padrão em um visualizador
     * pode ser útil para evitar renderizar coisas desnecessárias.
     * 
     */
    public function resetTemplates()
    {
        $this->templates = array();
    }

    /**
     * Métod que retorna se a página foi renderezida ou não.
     * 
     * @return boolean
     */
    public function isRender()
    {
        return self::$render;
    }

    /**
     * Método que informa ao visualizador que o mesmo já esta renderizado
     * dessa forma ele ignora uma renderização automática ao destruir um objeto.
     * 
     * Pode ser usado como uma alternativa em serviços web. 
     *
     * @param boolean $renderizado
     * @return void
     */
    public function setRenderizado($renderizado = true)
    {
        if ($renderizado) {
            ErrorHandler::$modoHTML = false;
        }
        self::$render = $renderizado;
    }

    /**
     * Método que mostra na tela o template adicionado em endereço
     *
     * @param string  $endereco = Endereco fisico do arquivo a ser aberto.
     */
    public function show($endereco)
    {
        try {
            $this->renderEngine->display($endereco);
        } catch (SmartyException $e) {
            $erro = new ErrorHandler();
            $erro->logarExcecao($e);
        }
        return;
    }

    /**
     * Retorna uma string com o resultado dos templates processados.
     * 
     * @return string página com as variáveis processadas
     */
    public function resultAsString()
    {
        $strResult = '';
        if (sizeof($this->templates) > 0) {
            foreach ($this->templates as $template) {
                $strResult .= $this->renderEngine->fetch($template);
            }
        }
        return $strResult;
    }

    /**
     * [DANGER] 
     * Retorna o objeto gerenciador de templates para modificar a biblioteca diretamente.
     * 
     * Verificar se existe algum método que atenda a sua necessidade e talvez uma contribuição com
     * o querido Eny vai bem.
     *
     * @return \core\view\templateEngine\TemplateEngine
     */
    public function getRenderEngine()
    {
        return $this->renderEngine;
    }

    /**
     * Adiciona um template com cabeçalho de formulário
     * 
     * @param string $action
     * @param string $id - default = formPrincipal
     */
    public function startForm($action, $id = 'formPrincipal')
    {
        $this->addTemplate(CORE . 'view/templates/generic/start_form');
        $this->addCSS('componentes/forms');
        $this->attValue('action', $action);
    }

    /**
     * Método que termina um formulário colocando os botões padrões de acordo com 
     * a lingua definida.
     * 
     * @param bool $botoes - se false não mostra os botões padrões
     */
    public function endForm($botoes = true)
    {
        if (!$botoes) {
            $this->attValue('WITHOUT_BUTTONS', true);
        }
        $this->addTemplate(CORE . 'view/templates/generic/end_form');
    }

    /**
     * 
     * @return OpenGraph
     */
    public function og()
    {
        return $this->og;
    }

    /**
     * Método que adiciona uma meta tag ao visualizador
     *
     * @param string $name
     * @param mixed $content
     * @return void
     */
    public function addMetaTag($name, $content)
    {
        $this->metaTags[$name] = $content;
    }


    /**
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Palavras chaves da página
     *
     * @return []
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    public function setDescription($description)
    {
        $this->description = Lang::get($description);
        $this->addMetaTag('twitter:description', $this->description); 
        return $this;
    }

    /**
     * Altera o Favicon padrão do sistema
     *
     * @param string $favicon
     * @return void
     */
    public function setFavicon($favicon)
    {
        $this->favicon = $favicon;
    }

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }

    public function addMensagemErro($mensagem)
    {
        $this->mensagemSistema->setTipoMensagem('ERRO');
        $this->mensagemSistema->setMensagem($mensagem);
    }

    public function addMensagemAlerta($mensagem)
    {
        $this->mensagemSistema->setTipoMensagem('ALERTA');
        $this->mensagemSistema->setMensagem($mensagem);
    }

    /**
     * 
     * @param mixed $erro
     */
    public function addErro($erro)
    {
        $this->mensagemSistema->addErro($erro);
    }

    public function addMensagemSucesso($mensagem)
    {
        $this->mensagemSistema->setTipoMensagem('SUCESSO');
        $this->mensagemSistema->setMensagem($mensagem);
    }

    /**
     * Adiciona um array com erros para ser mostrado na tela. 
     * Caso seja passado apenas uma string adiciona o erro na pilha de erros.
     * 
     * @param mixed $erro
     */
    public function addErros($erro)
    {
        $this->mensagemSistema->setTipoMensagem('ERRO');
        if (is_array($erro)) {
            $this->mensagemSistema->setMensagem('Os seguintes erros necessitam da sua atenção.');
            $this->mensagemSistema->addListaErro($erro);
        } else {
            $this->mensagemSistema->addErro($erro);
        }
    }

    /**
     * Método usado para processar os scripts dinamicos para renderização em 
     * ajax.  
     */
    public function addDinamicJS()
    {
        $this->attValue('selfScript', $this->varsJS . PHP_EOL . $this->selfScript);
    }

    /**
     * Mostra todos os templates que estão no Buffer de templates.
     */
    private function mostrarTemplatesNaTela()
    {
        if (sizeof($this->templates) > 0) {
            foreach ($this->templates as $template) {
                $this->show($template);
            }
        }
    }

    private function processaExtraMetatags()
    {
        if(is_file(ROOT . 'view/metatags.ini')){
            $iniFile = parse_ini_file(ROOT . 'view/metatags.ini', true);
            #todo metatags exclusivas??
            foreach($iniFile as $name => $content){
                $this->addMetaTag($name, $content);
            }
        }
    }

    private function processaCanonical(){
        if(defined('CANONICAL')){
            $end = $GLOBALS['URL'];
            $this->attValue('CANONICAL', CANONICAL . $end);
        }
    }

    private function attDefault()
    {
        $this->attValue('TITLE', $this->title);
        $this->attValue('DESCRIPTION', $this->description);
        $this->attValue('KEYWORDS', $this->keywords);
        $this->attValue('OPENGRAPH', $this->og);
        $this->attValue('FAVICON', $this->favicon);
        $this->attValue('PAGE', $this->page);

        $this->processaExtraMetatags();

        $this->attValue('METATAGS', $this->metaTags);

        //setCanonical
        $this->processaCanonical();
        //setLang
        $this->attValue('LANGUAGE', $this->getLang());

        $this->attCDN();
        $this->attCSS();
        $this->attJS();
    }

    private function attCSS()
    {
        $this->attValue('filesCSS', $this->filesCSS);
    }

    private function attJS()
    {
        $this->attValue('libsJS', $this->libJS);
        $this->attValue('scripts', $this->filesJS);
        $this->attValue('modules', $this->modulesJS);
        $this->attValue('selfScript', $this->varsJS . PHP_EOL . $this->selfScript);
        $this->attValue('selfScriptPos', $this->selfScriptPos);
    }

    private function attCDN()
    {
        $this->attValue('CDN', $this->CDN);
    }

    public function mensagemSystem(MensagemSistema $m = null)
    {
        $this->mensagemSistema = $m == null ? new MensagemSistema() : $m;
        $this->attValue('MSG', $this->mensagemSistema);
        $this->attValue('MSG_FILE', $this->mensagemSistema->getTemplate() . '.tpl'); //Variável para dar include do template
    }

    public function getMensagemSystem()
    {
        return $this->mensagemSistema;
    }

    /**
     * Método que adiciona o script básico do Enyalius e o JQuery
     *
     * @return void
     */
    private function baseJs()
    {
        $this->CDN()->add('jquery');
        $this->addLibJS('enyalius/main');
    }

    private function gtag()
    {
        if (!defined('GTAG')) {
            define('GTAG', '');
        }
        $this->attValue('GTAG', GTAG);
    }

    /**
     * Get the value of page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set the value of page
     *
     * @return  self
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }
}
