Eny.Modal = function(obj) {
    obj.type = obj.type ? obj.type : 'AJAX'  
    //adiciona a modal ID
    let tpl = $('#EnyModalTPL').html();
    tpl = Eny.processaTPL(tpl, obj, 'data');

    //Encapsula o ID para não poder ser alterado
    let ID = "#" + obj.id;

    //Expõe as configurações para ser alterado pelo JS
    this.conf = obj;

    this.remove =  false;

    $("body").append(tpl);

    this.eventAppend = function(){
        var funcaoAdicional = false;
        if(typeof arguments[0] != 'undefined' && typeof arguments[0] == 'function'){
            funcaoAdicional = arguments[0]; 
        }
        if (obj.type == 'AJAX') {
            $('.' + obj.classLink).on('click', function(e) {
                e.preventDefault();
                
                var title = $(this).attr('title');
                title = title ? title : obj.titulo;
                $(ID + ' .modal-title').html(title);
                var link = this;
                console.log($(this).attr('href'));
                $.get($(this).attr('href'), function(data, textStatus, jqXHR) {
                    $(`${ID} .modal-header`).html(data);
                    if(funcaoAdicional){                        
                        funcaoAdicional(link, obj)
                    }
                    obj.show = true;
                    $(ID).modal(obj);
                    


                    
                }).fail(function() {
                    alert( "Não foi possível encontrar a página!" );
                });    
    
                return false;
            });
        }
    }
    this.eventAppend();

    this.draggable  =  function(){
        $(ID).find('.modal-content')
        .css({
            width: 625,
            height: 175,
        })
        .draggable({
            handle: '.modal-header'
        });
     
        this.remove = true;

        
    }

    this.resizable = function(){
        $(ID).find('.modal-content')
        .css({
            width: 625,
            height: 175,
        })
        .resizable({
            minWidth: 625,
            minHeight: 175,
            handles: 'n, e, s, w, ne, sw, se, nw',
        });
        this.remove = true;
    }

    this.show = function(){
        $(ID).modal('show');
        if(this.remove){
            $(".modal-backdrop").remove();
        }
    }

    this.addButton = function({ id, text, classes, fn }) {
        const classStr = classes.join(' ')
        const botao = `<button id="${id}" class="${classStr}">${text}</button>`
        $(`${ID} .areaBotoes`).append(botao)
        $(`${ID} #${id}`).on('click', fn)
    }
};

/**
 * Método que reaplica o evento na modal geral. 
 * 
 * Talves seria interessante reaplicar a todas as modais.
 */