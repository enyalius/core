/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Eny Link Builder
 * 
 * @type type
 */
jQuery.extend($.fn.fmatter, {
    EnyLinkBuilder: function (valor, options, rowdata) {
        var base = '';
        if(options.colModel.options){
            base = options.colModel.options.baseLinkUrl;
        }
        //console.log(options);
        var link = base + valor; 
        return '<a href="' + link + '">' + valor + '</a>';
    }
});
jQuery.extend($.fn.fmatter.EnyLinkBuilder, {
    unformat: function (cellvalue, options) {
        return cellvalue.replace("$", "");
    }
});


jQuery.extend($.fn.fmatter, {
    EnySexo: function (valor, options, rowdata) {
        if (valor == 1) {
            return "Mas.";
        }
        if (valor == 2) {
            return "Fem."
        }
        return "ND."
    }
});
jQuery.extend($.fn.fmatter.EnySexo, {
    unformat: function (cellvalue, options) {
        return cellvalue.replace("$", "");
    }
});