//Requires Globais 
PackJS = require('./compiler/PackJS.js');
ListConcat = require('./compiler/ListConcat.js');

var concatLibs = new ListConcat();

var mapa = new PackJS('componentes/mapa', ['js/componentes/mapa/**/*.js']);
concatLibs.add(mapa);
concatLibs.addComponente('date_picker');
concatLibs.addComponente('chart');
concatLibs.addComponente('modal');
concatLibs.addComponente('splitjs');
concatLibs.addComponente('color_picker');
concatLibs.addComponente('frame_post');
concatLibs.add(new PackJS('enyalius', 'js/enyalius/**/*.js'));

var tabelaManterDados = new PackJS('componentes/tabela');
tabelaManterDados.prefix = 'js/componentes/tabela/';
tabelaManterDados.setOrigem(['tabela.js', 'tabela_manter_dados.js', 'formatos_colunas.js']);
tabelaManterDados.mainFile = 'tabela_manter_dados.js';
concatLibs.add(tabelaManterDados);



const tabelaRelatorio = new PackJS('componentes/tabela');
tabelaRelatorio.prefix = 'js/componentes/tabela/';
tabelaRelatorio.setOrigem(['tabela.js', 'tabela_relatorio.js']);
tabelaRelatorio.mainFile = 'tabela_relatorio.js';
concatLibs.add(tabelaRelatorio);

// Aqui nós carregamos o gulp e os plugins através da função `require` do nodejs
const gulp = require('gulp');
const concat = require('gulp-concat');
const merge = require('merge-stream');
const sass = require('gulp-sass')(require('sass'));
const cleanCSS = require('gulp-clean-css');
const terser = require('gulp-terser');


async function scripts() {
    console.log('--- Executando JSCompiller')
    var tasks = concatLibs.getList().map(function(element) {
        console.log("Gerando Lib " + element.src);
        return gulp.src(element.origens, { sourcemaps: true })
            .pipe(gulp.dest('/tmp/eny/debug' + element.getDebugFile()))
            .pipe(concat(element.mainFile)) //Une os arquivos
            .pipe(terser({}, terser.minify))
            .pipe(gulp.dest('/tmp/eny/js/' + element.getDistFile(), { sourcemaps: '/tmp/eny/js/maps/' }));
    });

    return merge(tasks);
}

function styles() {
    console.log('--- Executando SASS')
    return gulp.src(['css/*.scss', 'css/*.css', 'css/**/*.scss', 'css/**/*.css'])
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(cleanCSS())
        .pipe(gulp.dest('/tmp/eny/css'));
}

//######## watch ########
function watch() { // ,'browser-sync'
    // keep running, watching and triggering gulp
    gulp.watch('./js/**', scripts); //, reload
    gulp.watch('./css/**', styles); //, reload
};
var build = gulp.series(gulp.parallel(styles, scripts), watch);
exports.css = styles;
exports.distjs = scripts;
exports.watch = watch;
exports.build = build;
/*
 * Define default task that can be called by just running `gulp` from cli
 */
exports.default = build;