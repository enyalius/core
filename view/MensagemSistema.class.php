<?php

namespace core\view;

use core\view\Lang;
/**
 * Classe que armazena os textos que aparecerão nas mensagens do
 * sistema.
 * 
 * A classe tem por função controlar o tipo da mensagem, se é um erro, 
 * uma mensagem de sucesso e etc. Além disso ela armazena as mensagens que 
 * irão aparecer inclusive um array de erros.
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @package core.view
 * @version 11/05/2010
 */
class MensagemSistema
{

    
    private $ativa = false;
    private $lista = array();
    private $listaAlertas = array();

    private $tipoMensagem = 'alert-info';
    private $mensagem = '';
    private $template;

    public function __construct()
    {
        $this->template = CORE . 'view/templates/generic/message';
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

        
    /**
     * 
     * @param string $erro Mensagem de erro
     */
    public function addErro($erro)
    {
        $this->ativa = true;
        $this->lista[] = Lang::get($erro);

    }

    /**
     * 
     * @param array $lista
     */
    public function addListaErro($lista)
    {
        $this->addErro(Lang::get('Multiplos erros'));
        if (count($lista) > 0) {
            $this->ativa = true;
            if (count($this->lista) > 0) {
                $this->lista = array_merge($this->lista, $lista);
            } else {
                $this->lista = $lista;
            }
            return ;
        }
        throw new \ProgramacaoException("O argumento deve ser uma lista");
    }

    public function setMensagem($mensagem)
    {
        $this->ativa = true;
        $this->mensagem = Lang::get($mensagem);
    }

    public function getMensagem()
    {
        if($this->tipoMensagem == 'ERRO'){
            \_LOG::warning($this->mensagem);
        }
        return $this->mensagem;
    }

    public function getErros()
    {
        return $this->lista;
    }

    public function getAlertas()
    {
        return $this->listaAlertas;
    }

    public function isAtiva()
    {
        return $this->ativa;
    }
    
    public function getContent(){
        $mensagem  = $this->getMensagem();
        if(is_array($mensagem)){       
            $msg = '';
            foreach($mensagem as $sn){
                //imprime apenas 1 nível de array
                if(!is_array($sn)){
                    $msg .= Lang::get($sn);
                }
            }
            $mensagem = $msg;
        }
        $str = '<p>' . $mensagem. '</p>';
        if (sizeof($this->lista)) {
            $str .= '<ul>';
            foreach ($this->lista as $erro) {
                $str .= '<li>' . $erro . '</li>';
            }
            $str .= '</ul>';
        }
       
        return $str;
    }

    public function setTipoMensagem($tipoMensagem)
    {
        $tipoMensagem = strtoupper($tipoMensagem);
        if ($tipoMensagem == 'ERRO') {
            $this->tipoMensagem = 'alert-danger';
        } else if ($tipoMensagem == 'SUCESSO') {
            $this->tipoMensagem = 'alert-success';
        } else if ($tipoMensagem == 'ALERTA') {
            $this->tipoMensagem = 'alert-waring';
        } else {
            $this->tipoMensagem = 'alert-info';
        }
    }

    public function getTipoMensagem()
    {
        return $this->tipoMensagem;
    }

    /**
     * 
     * @return boolean true se existir alguma mensagem no buffer
     */
    public function isValida()
    {
        if ($this->mensagem != false || $this->ativa == true) {
            return true;
        } else {
            return false;
        }
    }

}
