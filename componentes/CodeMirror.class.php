<?php

/**
 * Componente que permite adicionar o CodeMirror nos projetos no enyalius.
 * Ele recebe um seletor Jquery e aplica a todos os textaareas que ele recebe
 * 
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class CodeMirror extends Componente
{
    use MagicData;

    protected static $adicionado = false;
    private static $cont = 0;
    private $seletor;
    private $lang;
    private $confs;

    private $theme = 'monokai';
    private $readOnly = false;
    private $mode = 'xml';

    /**
     * Cria uma tabela a partir de um seletor
     * @param string $seletor
     */
    public function __construct($seletor)
    {
        $this->seletor = $seletor;

        if (defined('LANG')) {
            $this->lang = strtoupper(LANG);
        }
    }

    public function add()
    {
        if (!self::$adicionado) {
            $this->view->addLibJS('codemirror', '/vendor/codemirror/lib/');
            $this->view->addLibJS($this->mode, '/vendor/codemirror/mode/' . $this->mode . '/');


            $this->view->addAbsoluteCSS('/vendor/codemirror/lib/codemirror.css');
            $this->view->addAbsoluteCSS('/vendor/codemirror/theme/' . $this->theme . '.css');



            self::$adicionado = true;
            $this->view->addSelfScript('var _codeMirrors = new Array();', true);

        }


        $this->view->addSelfScript('
            if ($("' . $this->seletor . '").length > 1 ) {
                _codeMirrors[' . self::$cont . '] = [];
                for( let i = 0; i < $("' . $this->seletor . '").length; i++){
                    _codeMirrors[' . self::$cont . '].push(CodeMirror.fromTextArea($("' . $this->seletor . '").get(i) , {
                                                                                    lineNumbers: true, 
                                                                                    mode: "' . $this->mode . '", 
                                                                                    lineWrapping: true, 
                                                                                    theme: "' . $this->theme . '",
                                                                                    readOnly: ' .  ($this->readOnly ? 'true' : 'false' ) . ', 
                                                                                }));
                }
            }else{
                _codeMirrors[' . self::$cont . '] =  CodeMirror.fromTextArea($("' . $this->seletor . '").get(0) , {
                    lineNumbers: true
                }); 
            }
          
          ', true);
        self::$cont++;
    }

    public function addConf($conf, $value)
    {
        $this->confs[$conf] = $value;
    }


    /**
     * Método que processa o vetor de configurações e adiciona na tabela
     *
     * @return string - objeto de configurações que podem ser adicionadas em tempo de execução
     */
    private function processaConfs()
    {
        $return = ', ';
        foreach($this->confs as $key =>$conf){
            $return .= $key . ' : ' . json_encode($conf) . ', ';
        }
        
        return rtrim($return, ', ');
    }

}
