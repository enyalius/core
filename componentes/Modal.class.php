<?php

/**
 * A classe Modal permite adicionar um componente default de modal.
 * Para ativar ele basta adicionar a classe "linkModal" a um link
 *
 * Ex: <a href="conteudoModal.html" class="linkModal">
 *
 * Para sistemas que forem usar recomenda-se carregar por padrão o componente.
 * O mesmo tem um baixo overread por possuir um pequeno script e um pequeno TPL.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @package core.componentes
 * @version 1.0.0 13/06/2021
 */
class Modal extends Componente
{

    protected static $adicionado = false;
    private static $cont = 0;
    private $id;
    private $confs = [];

    private $defaultClassLink = 'linkModal';
    private $tipo = 'AJAX';
    private $titulo = 'Sempre é bom saber mais!';
    private $labelButtonDismiss = 'Ok! Entendi!';

    private $resizable = false;
    private $draggable = false;

    private static $full = false;

    public function __construct($id = 'modalGeral')
    {
        $this->id = $id;
    }

    public function enableFull()
    {
        self::$full = true;
    }



    /**
     * Método que permite adicionar alguma configuração extra para a modal
     *
     * @param [type] $conf
     * @param [type] $value
     * @return void
     */
    public function addConf($conf, $value)
    {
        $this->confs[$conf] =  $value;
    }

    /**
     * Retorna o objeto que será convertido em JS
     *
     * @return stdClass
     */
    public function getObjectConf()
    {
        $conf = new stdClass();
        $conf->id = $this->id;
        $conf->tipo = $this->tipo;
        $conf->titulo = $this->titulo;
        $conf->labelButtonDismiss = $this->labelButtonDismiss;
        $conf->classLink = $this->defaultClassLink;
        $conf->full = self::$full;

        foreach ($this->confs as $key => $confValue) {
            $conf->{$key} = $confValue;
        }

        return $conf;
    }

    /**
     * Adicionar o componente na página
     *
     */
    public function add()
    {
        if (!self::$adicionado) {

            self::$adicionado = true;
            $this->view->addTemplate(CORE . 'view/templates/componentes/modal/modal');
            $this->view->addLibJS('componentes/modal/main');

            $this->view->addSelfScript('var enyModal = {};', true);
        }
        if( self::$full ){
            $this->view->addAbsoluteJS('https://code.jquery.com/ui/1.11.3/jquery-ui.min.js');
            $this->view->addAbsoluteCSS('https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css');            
        }

        $code  = 'enyModal.' . $this->id . ' =  new Eny.Modal( ' . json_encode($this->getObjectConf()) . ');';
        $code .= $this->extras();
        $this->view->addSelfScript( $code, true);
        self::$cont++;
    }

    private function extras()
    {
        $string = '';
        if($this->draggable){
            $string .= 'enyModal.' . $this->id . '.draggable();'; 
        }
        if($this->resizable){
            $string .=  'enyModal.' . $this->id . '.resizable();'; 
        }
        return $string;
    }

    /**
     * Set the value of defaultClassLink
     *
     * @return  self
     */
    public function setDefaultClassLink($defaultClassLink)
    {
        $this->defaultClassLink = $defaultClassLink;
        return $this;
    }

    /**
     * Set the value of titulo
     *
     * @return  self
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }



    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Set the value of labelButtonDismiss
     *
     * @return  self
     */
    public function setLabelButtonDismiss($labelButtonDismiss)
    {
        $this->labelButtonDismiss = $labelButtonDismiss;
        return $this;
    }


    
    /**
     * Habilita o draggable
     *
     */ 
    public function draggable()
    {
        $this->draggable = true;
        $this->enableFull();
        return $this;
    }

    /**
     * Habilita o resizable
     */ 
    public function resizable()
    {
        $this->resizable = true;
        $this->enableFull();
        return $this;
    }
}
