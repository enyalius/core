<?php
use core\view\cdn\CDNNode;


/**
 * A classe Split permite adicionar um divisor entre colunas ou linhas
 *
 * Implementado utilizando a lib https://split.js.org
 *
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @package core.componentes
 * @version 1.0.0 14/02/2022
 */
class Split extends Componente
{

    protected static $adicionado = false;
    private $id;
    private $divs;
    private $confs = [];
    private $sizes = [50, 50];
    private $direction = 'horizontal';
    private $defaultClassLink;

    /**
     * Divs que serão redirecionadas
     *
     * @param string $div1
     * @param string $div2
     */
    public function __construct($div1 = '#colunm-1', $div2 = '#colunm-2')
    {
        //implementar multiplas divs com multiplos args
        $this->divs = [$div1, $div2];
        $this->deps = [
            'js' => [[
                'cdn' => 'https://cdnjs.cloudflare.com/ajax/libs/split.js/1.6.5/split.min.js',
                'alternative' => '',
            ]],
        ];
        $storageID = rtrim(implode('_', $this->divs), '_');
        $storageID = str_replace('#', '', $storageID);
        $storageID = str_replace('.', '', $storageID);
        $this->id = $storageID;
    }

    public function setSizes($sizes){
        $this->sizes = $sizes;
    }
    

    public function getObjectConf()
    {
        $conf = new stdClass();
        $conf->ids = $this->divs;
        $conf->sizes = $this->sizes;
        if($this->direction != 'horizontal'){
            $conf->direction = 'vertical';
        }        
        $conf->storageID = $this->id;
        return $conf;
    }

    /**
     * Adicionar o componente na página
     *
     * @return void
     */
    public function add()
    {
        if (!self::$adicionado) {
            self::$adicionado = true;
            $this->getDeps();
            $this->view->addLibJS('componentes/splitjs/main');
            $this->view->addCSS('componentes/splitjs/split');
            $this->view->addSelfScript('var enySplit = {};', true);
        }
        $this->view->addSelfScript('enySplit.' . $this->id . ' =  new Eny.Split( ' . json_encode($this->getObjectConf()) . ');', true);
    }

    /**
     * Set the value of defaultClassLink
     *
     * @return  self
     */
    public function setDefaultClassLink($defaultClassLink)
    {
        $this->defaultClassLink = $defaultClassLink;
        return $this;
    }

    public function getDeps()
    {
        if (!is_null($this->deps)) {
            foreach ($this->deps['js'] as $el) {
                $this->view->CDN()->addNode(CDNNode::makeSimpleNode(get_class(), $el['alternative'], $el['cdn']));
            }
        }
    }

    /**
     * Set the value of direction
     *
     * @return  self
     */ 
    public function setVerticalDirection()
    {
        $this->direction = 'vertical';
        return $this;
    }
}
