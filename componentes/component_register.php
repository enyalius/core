<?php

Componente::registraComponente('TabelaManterDados', 'tabelas');
Componente::registraComponente('Tabela', 'tabelas');
Componente::registraComponente('TabelaColuna', 'tabelas');
Componente::registraComponente('TabelaConsulta', 'tabelas');
Componente::registraComponente('TabelaRelatorio', 'tabelas');
Componente::registraComponente('DataTable', 'tabelas');

Componente::registraComponente('Chart', 'graficos');
Componente::registraComponente('HTMLEditor', 'htmlEditor');


Componente::registraComponente('Mapa', 'mapa');
