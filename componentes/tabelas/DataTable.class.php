<?php

/**
 * Componente que permite adicionar o DataTable nos projetos no enyalius.
 * 
 * Doc padrão: https://datatables.net
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class DataTable extends Componente
{
    protected static $adicionado = false;
    private static $cont = 0;
    private $lang = 'PT-BR';
    private $export = false;
    private $confs = [];
    private $id;


    /**
     * Cria uma tabela a partir de um ID informado
     *
     * @param string $id
     */
    public function __construct($id)
    {
        $this->id = $id;

        if (defined('LANG')) {
            $this->lang = strtoupper(LANG);
        }
    }

    public function add()
    {
        if (!self::$adicionado) {
            $this->view->addLibJS('jquery.dataTables.min', '/vendor/datatables.net/js/');

            $this->view->addAbsoluteCSS('/vendor/datatables.net-dt/css/jquery.dataTables.min.css');


            self::$adicionado = true;
            $this->view->addSelfScript('var dataTables = new Array();', true);
        }
        $this->view->addSelfScript('dataTables[' . self::$cont . '] =   $("' . $this->id . '").DataTable(' . $this->makeConfObject() . ');
        $("' . $this->id . '").addClass("table-bordered");
        ', true);
        self::$cont++;
    }

    public function addConf($conf, $value)
    {
        $this->confs[$conf] = $value;
    }

    public function allExport()
    {
        $this->export = ['copy', 'csv', 'excel', 'print'];
    }

    public function addExport($export)
    {
        $this->export[] = $export;
    }

    private function makeExport()
    {
        if ($this->export) {
            $this->view->addAbsoluteJS('https://cdn.datatables.net/buttons/2.0.0/js/dataTables.buttons.min.js');
            $this->view->addAbsoluteJS('https://cdn.datatables.net/buttons/2.0.0/js/buttons.print.min.js');
            $this->view->addAbsoluteJS('https://cdn.datatables.net/buttons/2.0.0/js/buttons.html5.min.js');
            //Excel
            $this->view->addAbsoluteJS('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js');
            //pdf Não funciona ainda
            // $this->view->addAbsoluteJS('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js');
            // $this->view->addAbsoluteJS('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js');

            //CSS
            $this->view->addAbsoluteCSS('https://cdn.datatables.net/buttons/2.0.0/css/buttons.dataTables.min.css');


            return ', dom: "Bftflip", buttons:' . json_encode($this->export);
        }
    }

    /**
     * Método que processa o vetor de configurações e adiciona na tabela
     *
     * @return string - objeto de configurações que podem ser adicionadas em tempo de execução
     */
    private function processaConfs()
    {
        $return = ', ';
        foreach($this->confs as $key =>$conf){
            $return .= $key . ' : ' . json_encode($conf) . ', ';
        }
        
        return rtrim($return, ', ');
    }

    /**
     * Método que monta o DataTable padrão do Eny
     *
     * @return string - String representando um objeto JavaScript
     */
    private function makeConfObject()
    {
        $obj =
            '{'
            . $this->getLanguage() 
            . $this->makeExport()
            . $this->processaConfs()        
            . '}';


        return $obj;
    }

    private function getLanguage()
    {
        $lang = 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/en-gb.json';
        if ($this->lang == 'PT' || $this->lang == 'PT-BR') {
            $lang = 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/pt_br.json';
        } else if ($this->lang == 'ES') {
            $lang = 'https://cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json';
        }

        return '"language": {
            url: "' . $lang . '"
        }';

        ////cdn.datatables.net/plug-ins/1.11.1/i18n/es_es.json
        //cdn.datatables.net/plug-ins/1.11.1/i18n/en-gb.json
    }


    /**
     * Set the value of lang
     *
     * @return  self
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }
}
