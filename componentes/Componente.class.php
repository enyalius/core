<?php
use core\view\cdn\CDNNode;

/**
 * Classe abstrata que determina os métodos que um componente da interface gráfica deve
 * possuir
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 2.0.0
 * @package core.components
 */
abstract class Componente
{
    
    /**
     * @var \AbstractView
     */
    protected $view;
    protected static $components = array();
    protected static $i = 1;
    protected static $adicionado = false;
    protected $deps = null;


    /**
     * Adiciona o componente na tela.
     */
    public abstract function add();

    public function requisitaComponente($componente)
    {
        #TODO vinculado a ISSUE#4
    }

    public function getView()
    {
        return $this->view;
    }

    public function setView(\AbstractView $view)
    {
        $this->view = $view;
        return $this;
    }

    public static function registraComponente($componente, $pathAdicional = '')
    {
        $path = CORE . 'componentes/' . $pathAdicional . '/' . $componente;
        self::$components[$componente] = $path;
    }

    /**
     * Carrega um commonente para o loader. 
     *
     * @param [type] $componente
     * @return void
     */
    public static function load($componente)
    {
        if (!isset(self::$components[$componente])) {
            self::registraComponente($componente);
            //throw new Exception("Componente requisitado não registrado!", 1);
        }
        $loader = $GLOBALS['loader'];
        $loader->addClass($componente, self::$components[$componente]);
    }

    /**
     * Undocumented function
     *
     * @param [type] $componente
     * @deprecated 2.0 use "load"
     * @return void
     */
    public static function carregaComponente($componente)
    {
        return self::load($componente);
    }
    
    protected function addTemplate($template){
        $this->view->addTemplate(CORE . 'view/templates/componentes/' . $template, true);
    }
    
    protected function generateId(){
        return self::$i++;
    }
    
    /**
     * 
     * #TODO ver de integrar ao CDN
     * 
     * @param string $path
     * @param string $js
     */
    protected function addVendorJS($path, $js ){
        if(!is_file(PUBLIC_DIR . '/' . $path)){
            throw new ProgramacaoException('Necessário adicionar a dependencia externa no Yarn do ' . $path);
        }
        $this->view->addLibJS($js, $path);
    }

}
