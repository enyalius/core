<?php


/**
 * A classe Captcha permite integrar o ReCaptcha do google de forma fácil em qualquer página
 * 
 * Adicionar no botão de submit o seguinte código 
 * 
 * 
    <button class="g-recaptcha" 
        data-sitekey="{$CAPTCHA_CLIENT_API}" 
        data-callback="onSubmit" 
        data-action="submit">Submit</button>

 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @package sistema.controlador.componentes
 * @version 1.0.0 16/12/2022
 */
class Captcha extends Componente{
    private $id;

    private $clientKey;

    public function __construct($clientKey = CAPTCHA_CLIENT_API, $id = 'formPrincipal'){
        $this->id = $id;
        $this->clientKey = $clientKey;
    }

    /**
     * Undocumented function
     *
     * @param string $apiKey - key do servidor utilizado para validar
     * @return void
     */
    public static function verify($apiKey){
        //Pego a validação do Captcha feita pelo usuário
        $recaptcha_response = $_POST['g-recaptcha-response'];

        // Verifico se foi feita a postagem do Captcha 
        if(isset($recaptcha_response)){
            $answer = 
                json_decode(
                    file_get_contents(
                        'https://www.google.com/recaptcha/api/siteverify?secret=' . $apiKey .
                        '&response='.$_POST['g-recaptcha-response']
                    )
                );
                if($answer->success) {
                    return true;
                }
            }
            return false;
    }
    
    public function setLista($lista){
        if(is_array($lista)){
            
        }else{
            throw new \ProgramacaoException('O argumento $lista deve ser uma lista');
        }
    }
    
    
    public function add() {
        if(!self::$adicionado){
            self::$adicionado = true;
            $this->view->addAbsoluteJS('https://www.google.com/recaptcha/api.js');
        }
        
        $this->view->attValue('CAPTCHA_API', $this->clientKey);
        $this->view->addSelfScript(' 
          function onSubmit(token) {
            document.getElementById("' .$this->id. '").submit();
          }
       ');

    }
}
