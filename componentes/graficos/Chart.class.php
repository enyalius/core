<?php

/**
 * Grafico de linha
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class Chart extends Componente
{
    protected static $adicionado = false;
    private static $cont = 0;
    private $chartModel;

    public static function loadDeps()
    {
        Componente::registraComponente('ChartModel', 'graficos');
        Componente::registraComponente('DatasetChart', 'graficos');

        Componente::load('DatasetChart');
        Componente::load('ChartModel');
    }

    public function __construct(ChartModel $chart)
    {
        $this->chartModel = $chart;
    }

    public function add()
    {
        if (!self::$adicionado) {
            $this->view->addLibJS('chart.min', '/vendor/chart.js/dist/');
            $this->view->addLibJS('componentes/chart/main');

            self::$adicionado = true;
            $this->view->addSelfScript('var enyCharts = new Array();', true);
        }
        $this->view->addSelfScript('enyCharts[' . self::$cont . '] =  new Eny.Chart(' . json_encode($this->chartModel).');', true);
        self::$cont++;
    }

}
