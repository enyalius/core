<?php

class DatasetChart implements JsonSerializable{

    private $title;
    private $data = [];
    private $color = '#FF0000';
    private $especialColor = false;
    

    public function jsonSerialize():mixed
    {
        return [
            'label' => $this->title,
            'data' => $this->data,
            'borderColor' => $this->color,
            'backgroundColor' => $this->getBackgroundColor(),
            'barPercentage' => 1,
            'outlierColor' => '#999999',
            'borderWidth' => 1,
            'categoryPercentage' => 0.9,
        ];
    }

    public function setEspecialBackgroundColor($color){
        $this->especialColor = $color;
    }
    private function getBackgroundColor(){
        return $this->especialColor ? $this->especialColor : $this->color;
    }

    public function data($data){
        $this->data = $data;
    }

    public function addData($data){
        $this->data[] = $data;
    }

    /**
     * Get the value of title
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */ 
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get the value of color
     */ 
    public function getcolor()
    {
        return $this->color;
    }

    /**
     * Set the value of color
     *
     * @return  self
     */ 
    public function setcolor($color)
    {
        $this->color = $color;
        return $this;
    }
}