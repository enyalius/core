<?php

/**
 * Classe com métodos 
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.1
 * @package util
 */
class DateUtil
{

    public static function monthYear($input, $format = 'Y-m-d')
    {
        if (empty($input)) {
            return false;
        }

        $date = DateTime::createFromFormat($format, $input);
        return $date->format('m/Y');
    }

    /**
     * Método que pega uma data em formatos típicos do Brasil 
     * e converte para o formato YYYY-m-d
     * 
     * @param string $date
     * @param bool $obrigatorio 
     * @return boolean|string
     */
    public static function preparaData($date, $obrigatorio = true)
    {
        if (empty($date) && !$obrigatorio) {
            return null;
        }

        if (empty($date) && $obrigatorio) {
            return false;
        }

        $dateTest = DateTime::createFromFormat('Y-m-d', $date);
        if ($dateTest) {
            return (string) $dateTest->format('Y-m-d');
        }

        $dateTest = DateTime::createFromFormat('d/m/Y', $date);
        if ($dateTest) {
            return $dateTest->format('Y-m-d');
        }

        $dateTest = DateTime::createFromFormat('d/m/y', $date); //padrão do date com ano em 2 digitos
        if ($dateTest) {
            return $dateTest->format('Y-m-d');
        }
        return false;
    }

    public static function getMeses()
    {
        return array(
            1 => 'Janeiro',
            2 => 'Fevereiro',
            3 => 'Março',
            4 => 'Abril',
            5 => 'Maio',
            6 => 'Junho',
            7 => 'Julho',
            8 => 'Agosto',
            9 => 'Setembro',
            10 => 'Outubro',
            11 => 'Novembro',
            12 => 'Dezembro'
        );
    }

    public static function getDiasSemana()
    {
        return array(
            0 => 'Domingo',
            1 => 'Segunda-feira',
            2 => 'Terça-feira',
            3 => 'Quarta-feira',
            4 => 'Quinta-feira',
            5 => 'Sexta-feira',
            6 => 'Sabado'
        );
    }

    public static function getMes($num)
    {
        $num = 0 + $num;
        $meses = self::getMeses();
        return $meses[$num];
    }

    public static function getDiaSemana($num)
    {

        $dias = self::getDiasSemana();
        return $dias[$num];
    }

    public static function getAnos($menos)
    {
        $anos = array();
        $ano = date('Y');
        for ($i = $ano; $i > ($ano - $menos); $i--) {
            $anos[$i] = $i;
        }
        return $anos;
    }

    public static function formataData($data, $comHora = false)
    {
        //echo $data;
        if (empty($data)) {
            return '';
        }
        $dataLocal = explode(' ', $data);
        $hora = isset($dataLocal[1]) ? $dataLocal[1] : '';
        $data = explode('-', $dataLocal[0]);
        if ($data[2] > 1000) {
            $data = $data[0] . '/' . $data[1] . '/' . $data[2];
        } else {
            $data = $data[2] . '/' . $data[1] . '/' . $data[0];
        }
        if ($hora != '00:00:00' && $comHora) {
            $hora = explode(':', $hora);
            $hora = $hora[0] . ':' . $hora[1];
            return $data . ' às ' . $hora;
        } else {
            return $data;
        }
    }

    public static function apenasHora($data)
    {
        if (empty($data)) {
            return '';
        }
        list($x, $hora) = explode(' ', $data);
        $hora = explode(':', $hora);
        $hora = $hora[0] . ':' . $hora[1];
        return $hora;
    }

    /**
     * Formata a data para mostar a informação em formato PT-BR  
     *
     * @param $dataOriginal Data em formato YYYY-MM-DD para ser formatada.
     */
    public static function formataDataExtenso($dataOriginal, $comHora = false)
    {

        $parts = explode(' ', $dataOriginal);
        $data = isset($parts[0]) ? $parts[0] : '';
        $hora = isset($parts[1]) ? $parts[1] : '';
        $data = explode('-', $data);
        if (!isset($data[2])) {
            return $dataOriginal;
        }

        $data = $data[2] . ' de ' . self::getMes($data[1]) . ' de ' . $data[0];
        if ($hora != '00:00:00' && $comHora) {
            $hora = explode(':', $hora);
            $hora = $hora[0] . ':' . $hora[1];
            return $data . ' às ' . $hora;
        } else {
            return $data;
        }
    }
}
