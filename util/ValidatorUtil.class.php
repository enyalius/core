<?php

/**
 * Classe permite fazer algumas validações dificultando Injection e XSS
 * 
 * #TODO análisar mais possíveis melhorias
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package core.util 
 */
abstract class ValidatorUtil
{

    /**
     * Método que valida se o nome pode ser uma variável
     * 
     * @param mixed $var
     * @return string 
     */
    public static function variavel($var)
    {
        $var = trim($var??'');
        return preg_replace('[^aA-zZ/0-9çãõéíóáúêô_]', '', $var);
    }

    public static function sanitizeAndDecodeJsonInput()
    {
        // Lê o corpo da requisição
        $payload = file_get_contents('php://input');

        // Converte o JSON em um array associativo (defina 'true' para array, ou 'false' para objeto)
        return JSON::decode($payload, true);

       
    }

    /**
     * Método que utiliza o filter sanitize para tratar uma variavel do tipo int
     * 
     * @param mixed $var
     * @return int 
     */
    public static function variavelInt($var)
    {
        $var = filter_var($var, FILTER_SANITIZE_NUMBER_INT);
        return (int) $var;
    }

    /**
     * Método que deixa uma string safe para evitar ataques com strings maliciosas.
     * 
     * Basicamente remove todo o HTML do texto não usar quando se quer utilizar HTML
     * 
     * @param mixed $var
     * @return string 
     */
    public static function stringVar($var)
    {
        $str = preg_replace('/\x00|<[^>]*>?/', '', $var);
        return $str;
    }

    /**
     * Recebe um array e trata os dados o método é recursivo caso receba um 
     * array.  
     * 
     * Lembre-se que é melhor tratar a saída e não a entrada. 
     * A entrada é mais perigosa para o banco. 
     * 
     * @param array $array
     * @param array $ignore
     * @return array
     */
    public static function trataArrayDados($array, $ignore = [])
    {
        /*if(is_array($ignore) && sizeof($ignore) > 0){
            $ignore = array_merge(...$ignore); //Caso tenha sido passado um array multidimensional achata o mesmo
        }*/
        foreach ($array as $key => $value) {
            if (in_array($key, $ignore)) {
                continue;
            }
            if (is_array($value)) {
                $array[$key] = self::trataArrayDados($value, $ignore);
            } else if (is_int($value)) {
                $array[$key] = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
            } else if (is_float($value)) {
                $array[$key] = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            } else if (is_string($value)) {   
                //https://stackoverflow.com/questions/69207368/constant-filter-sanitize-string-is-deprecated
                $str = preg_replace('/\x00|<[^>]*>?/', '', $value);
                $array[$key] = str_replace(["'", '"'], ['&#39;', '&#34;'], $str);              
            } else {
                $array[$key] = filter_var($value, FILTER_DEFAULT);
                continue;
            }
        }
        return $array;
    }

    /**
     * 
     * @param mixed $ignore
     * @return array
     */
    public static function sanitizeForm(...$ignore)
    {
        //ds($ignore);
        $dados = self::trataArrayDados($_POST, $ignore);
        return $dados;
    }
}
