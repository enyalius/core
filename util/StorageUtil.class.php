<?php

/**
 * Classe Util que permite manipular a session 
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package core.util
 */
abstract class StorageUtil
{
 
    /**
     * Adiciona um dado na sessão 
     * 
     * @param string $key
     * @param mixed $value
     */
    public static function add($key, $value)
    {
        if (is_object($value)) {
            $_SESSION['storage'][$key]['object'] = serialize($value);
        } else {
            $_SESSION['storage'][$key] = $value;
        }
    }

    

    /**
     * Método que retorna uma variável salva na sessão.
     * 
     * Retorna nulo em caso de não existencia
     * 
     * @param string $key
     * @param mixed $default valor a retornar caso não encontre o dado
     * @return mixed
     */
    public static function get($key, $default = null)
    {
        if (isset($_SESSION['storage'][$key])) {
            if (isset($_SESSION['storage'][$key]['object'])) {
                return unserialize($_SESSION['storage'][$key]['object']);
            }
            return $_SESSION['storage'][$key];
        }
        return $default;
    }

    /**
     * Verifica se a key está setada
     *
     * @param string $key
     * @return boolean
     */
    public static function verify($key)
    {
        return isset($_SESSION['storage'][$key]);
    }

    /**
     * Deleta o campo da sessão
     * 
     * @param string $key
     */
    public static function del($key)
    {
        unset($_SESSION['storage'][$key]);
    }

}
