<?php

use core\view\Lang;

$phpVersion = (float)phpversion();


if ($phpVersion >= 5.6) {
    require_once __DIR__. '/HELPER5_6.php';
} else {
    function ds($args) {
        foreach ($args as $a) {
            DebugUtil::show($a);
        }
        var_dump(debug_backtrace());
    }

    /**
     * 
     */
    function dd($args) {
        foreach ($args as $a) {
            DebugUtil::show($a);
        }
        var_dump(debug_backtrace());
        exit();
    }     
}

/**
 * Ativa o debug de ações no banco de dados.
 *
 * @return void
 */
function dbd(){
    BDHelper::debugALL();
}

/**
 * Helper para usar a tradução do Enyalius. 
 * 
 * Explorar a utilização do gettext
 *
 * @param string $texto
 * @return void
 */
function __($texto){
    return Lang::get($texto);
}

function tplSelector($tpl){
    $template = preg_replace('/\.tpl$/', '', $tpl);
    $lang = Lang::$lang;    
    if (!empty($lang)) {
        if (is_file(TEMPLATES . $template . '_' . $lang . '.tpl')) {
            return $template . '_' . $lang . '.tpl';
        }
        if (is_file(TEMPLATES . 'i18n/' . $lang . '/' . $template . '.tpl')) {
            return TEMPLATES . 'i18n/' . $lang . '/' . $template . '.tpl';
        }
    }

    return $template . '.tpl';
}