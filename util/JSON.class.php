<?php

/**
 * Classe que permite uma codificação em JSON realizando alguns testes e exibindo as mensagens de texto correto.
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class JSON
{

    /**
     * 
     * @param mixed $value
     * @param int $options
     * @param int $depth
     * @param boolean $utfErrorFlag
     * @return string
     */
    public static function encode($value, $options = 0, $depth = 1024, $utfErrorFlag = false)
    {
        $encoded = json_encode($value, $options, $depth);
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return $encoded;
            case JSON_ERROR_DEPTH:
                //throw new JSONErrorException();
                return 'Maximum stack depth (' . $depth . ') exceeded'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_STATE_MISMATCH:
                return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_CTRL_CHAR:
                return 'Unexpected control character found';
            case JSON_ERROR_SYNTAX:
                return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_UTF8:
                $clean = self::utf8ize($value);
                if ($utfErrorFlag) {
                    return 'UTF8 encoding error'; // or trigger_error() or throw new Exception()
                }
                return self::encode($clean, $options, $depth, true);
            default:
                return 'Unknown error'; // or trigger_error() or throw new Exception()
        }
    }

    private static function utf8ize($array)
    {
        #Método obtido no site: http://nazcalabs.com/blog/convert-php-array-to-utf8-recursively/
        array_walk_recursive($array, function(&$item, $key) {
            if(is_string($item)){
                if (!mb_detect_encoding($item, 'utf-8', true)) {
                    $item = utf8_encode($item);
                }
            }
        });
        return $array;
    }

    public static function getLastError($asString = FALSE)
    {
        $lastError = \json_last_error();

        if (!$asString)
            return $lastError;

        // Define the errors.
        $constants = \get_defined_constants(TRUE);
        $errorStrings = [];

        foreach ($constants["json"] as $name => $value)
            if (!strncmp($name, "JSON_ERROR_", 11))
                $errorStrings[$value] = $name;

        return isset($errorStrings[$lastError]) ? $errorStrings[$lastError] : FALSE;
    }

    public static function getLastErrorMessage()
    {
        return \json_last_error_msg();
    }

    public static function clean($jsonString)
    {
        if (!is_string($jsonString) || !$jsonString)
            return '';

        $jsonString = trim( (string) $jsonString);
        $jsonString = preg_replace('/\s+/', ' ', $jsonString);
        $jsonString = stripslashes($jsonString);
        
        return $jsonString;
    }

    public static function decode($jsonString, $asArray = false, $depth = 512, $options = JSON_BIGINT_AS_STRING)
    {
        if (!is_string($jsonString) || !$jsonString)
            return NULL;
   

        $result = \json_decode($jsonString, $asArray, $depth, $options);

        if ($result === NULL)
            switch (self::getLastError()) {
                case JSON_ERROR_SYNTAX :
                    // Try to clean json string if syntax error occured
                    $jsonString = self::clean($jsonString);
                   

                    $result = \json_decode($jsonString, $asArray, $depth, $options);
                    break;

                default:
                
            }
            
        return $result;
    }

}
