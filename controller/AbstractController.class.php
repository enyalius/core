<?php

/**
 * Model of Controller of system
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.1
 * @package core.controller 
 */
#[AllowDynamicProperties]
abstract class AbstractController
{

    use \core\controller\StorageTrait;
    use \core\controller\ToolsTrait;

    /**
     *
     * @var AbstractView  
     */
    protected $view;

    /**
     * Método padrão chamado para o controlador caso a ação não seja especificada
     */
    public abstract function index();

    /**
     * Método que executa caso encontra um controlador mas não a sua ação
     * 
     * @return void
     */
    public abstract function paginaNaoEncontrada();

    /**
     * 
     * @param string $url
     */
    public function redirect($url)
    {
        if (!empty($url)) {
            header('Location: ' . $url);
        }
    }

    /**
     * Retorna o objeto de view para proteger a troca.
     *
     * @return AbstractView
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Captura todos os argumentos extras passados para o método via URL
     * 
     * @param int $pos posição do argumento extra começa com 0
     * @param int $filter Filtro de Sanitize que pode ser utilizado por padrão Trata uma string
     * @return mixed
     */
    protected function getArg($pos, $tipo = FILTER_UNSAFE_RAW)
    {
        if (isset($GLOBALS['ARGS'][$pos])) {
            return filter_var($GLOBALS['ARGS'][$pos], $tipo);
        }
        return '';
    }

    /**
     * Array com os argumentos passados na URL.
     *
     * @return array
     */
    protected function getArgs()
    {
        if (isset($GLOBALS['ARGS'])) {
            return $GLOBALS['ARGS'];
        }
        return [];
    }

    /**
     * Método que permite adicionar um argumento para demais métodos. 
     * Útil em redirects
     *
     * @param mixed $value
     * @param [int] $pos
     */
    protected function putArg($value, $pos = false)
    {
        if ($pos !== false) {
            $GLOBALS['ARGS'][$pos] = $value;
        } else {
            $GLOBALS['ARGS'][] = $value;
        }
    }

    /**
     * Método que remove algum arg para passar de um método para outro.
     *
     * @param [type] $pos
     */
    protected function unsetArg($pos)
    {
        unset($GLOBALS['ARGS'][$pos]);
    }

    /**
     * Atalho para facilitar acesso a IDs 
     *
     * @param int $pos
     * @return int
     */
    protected function getIntArg($pos)
    {
        $val =  $this->getArg($pos, FILTER_SANITIZE_NUMBER_INT);
        if ($val === '') {
            return false;
        }
        return $val;
    }

    /**
     * Garante o retorno de um Int.
     * 
     * @param mixed $value
     * @return int
     */
    protected function trataInt($value)
    {
        return filter_var($value, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Trata o campo para não aceitar uma string maliciosa.
     * 
     * O Ideal é sempre usar isso no Output e não no input.
     * 
     * @param mixed $value
     * @return string
     */
    protected function trataString($value)
    {
        return ValidatorUtil::stringVar($value);
    }

    /**
     * Verifica se a váriavel POST existe e a trata de forma adequada para uma string.
     * 
     * Atenção: Caso queira usar valores HTML é necessário mudar a variável opcional FILTER 
     * 
     * @param string $indice
     * @param int [$filter] Filtro de Sanitize que pode ser utilizado por padrão Trata uma string
     * @return mixed Valor da variavel null no caso de não existir
     */
    protected function trataPost($indice, $filter = FILTER_UNSAFE_RAW)
    {
        return filter_input(INPUT_POST, $indice, $filter);
    }

    /**
     * Verifica se a váriavel GET existe e a trata de forma adequada para uma string.
     * 
     * Atenção: Caso queira usar valores HTML é necessário verificar a variável de outra maneira 
     * 
     * @param string $indice
     * @param int [$filter] Filtro de Sanitize que pode ser utilizado por padrão Trata uma string
     * @return mixed Valor da variavel null no caso de não existir
     */
    protected function trataGet($indice, $filter = FILTER_UNSAFE_RAW)
    {
        return filter_input(INPUT_GET, $indice, $filter);
    }

    /**
     * Método que verifica se o request é um POST.
     *
     * @return boolean
     */
    public final function isPost()
    {
        return ($_SERVER['REQUEST_METHOD'] == 'POST');
    }


    /**
     * Método que retorna 400 
     * 
     * @param mixed $content
     */
    protected function error400($content)
    {
        $this->response($content, 400);
    }

    /**
     * 
     * @param mixed $content
     */
    protected function response200($content)
    {
        $this->response($content);
    }

    protected function response($content, $code = 200)
    {
        $this->setRenderizado();
        http_response_code($code);
        if (is_array($content) || is_object($content)) {
            //header('Content-Type: application/json; charset=utf-8'); #precisa repassar varias funções no revisão para evitar erro no cliente
            echo JSON::encode($content);
        } else {
            echo $content;
        }
        exit();
    }

    /**
     * Coloca a view em modo renderizado
     *
     * @return void
     */
    protected function setRenderizado()
    {
        if (isset($this->view) && !$this->view->isRender()) {
            $this->view->setRenderizado();
        }
    }

    /**
    * Verifica se é um insert em cascade ou seja se é necessário voltar 
    * para um insert em andamento.
    * 
    * #TODO refazer com session e mensagens
    */
    protected function insertCascade()
    {
        $redirect = false;
        if (!isset($_REQUEST['redirect'])) {
            $routePattern = '/^[a-zA-Z0-9\/]+$/';
            if (preg_match($routePattern, $_GET['redirect'])) {
                $this->redirect('/' . $this->getArg(0) . '/criarNovo');
            }
        }
    }

    protected function redirectIfNoData($data, $redirect = '/')
    {
        if(!isset($_REQUEST[$data])){
            $this->redirect($redirect);
        }
    }


    public function __destruct()
    {
        if (isset($this->view) && !$this->view->isRender()) {
            $this->view->render();
        }
    }

    /**
     * Exibe o logo do Enyalius para o tools e para créditos
     *
     * @return void
     */
    public function logoEny()
    {
        $this->view->setRenderizado();
        header('Content-type: image/svg+xml');
        echo file_get_contents(CORE . 'doc/Logotipia/eny.svg');
    }

    /**
     * Informações sobre a versão do Enyalius
     *
     * @return void
     */
    public function aboutEny()
    {
        if (DEBUG) {
            $string = file_get_contents(CORE . 'composer.json');
            $data = JSON::decode($string, true);
            echo 'Versão: ' . $data['version'];
            echo '<br> ';
            echo 'Description: ' . $data['description'];
            echo '<br> '; echo '<br> ';
            
            echo '<br> Mais informações: <a href="https://gitlab.com/enyalius">https://gitlab.com/enyalius</a>';
            echo '<br><img src="/logoEny" height="200" width="300" /> ' ;
            $this->view->setRenderizado();
        }
    }
}
