<?php

namespace core\controller;

/**
 * Trait que permite carregar uma ferramenta disponível no projeto Tools do Enyalius
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package core.controller
 */
trait ToolsTrait
{

    /**
     * Método que executa a tool gerador de CRUD.
     * 
     * Vale lembrar que o submodulo tools não for clonado esse método não tem serventia.[
     */
    public function tool()
    {
        if (!(isset($GLOBALS['ARGS']['0']) && !empty($GLOBALS['ARGS']['0']) && $this->carregaTool($GLOBALS['ARGS']['0']))) {
            $this->eny();
        }
    }

    public function eny()
    {

        if(DEVEL){
            return $this->carregaTool('GeradorDeCrud');
        } 
        $this->paginaNaoEncontrada();
    }

    private function carregaTool($class)
    {
        if (!file_exists(CORE . '../tools/' . $class . '.class.php')) {
            return false;
        }
        if (DEBUG) {
            $this->view->setRenderizado();

            require_once(CORE . '../tools/' . $class . '.class.php');
            new $class();
            return true;
        } else {
            $this->paginaNaoEncontrada();
        }
    }

}