<?php

class MinerModel
{

    private $server = 'http://localhost/';
    private $port = false;
    private $service = false;
    private $textField = 'text';
    private $confs = [];
    private $options = [
        'http' => [
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
        ]
    ];
    private $file;

    public function __set($name, $value)
    {
        $realPrivate = ['confs'];
        if(!in_array($name, $realPrivate)){
            $metodoName = 'set' . ucfirst($name);
            if(method_exists($this, $metodoName)){
                $this->{$metodoName}($value);
            }
            $this->{$name} = $value;
            return $this;
        }
            $trace = debug_backtrace();
            trigger_error(
                'Undefined property via __get(): ' . $name .
                    ' in ' . $trace[0]['file'] .
                    ' on line ' . $trace[0]['line'],
                E_USER_NOTICE
            );
        
    }

    public function __get($name)
    {
        if (isset($this->{$name}) && !method_exists($this, 'get' . ucfirst($name))) {
            return $this->{$name};
        }
        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
                ' in ' . $trace[0]['file'] .
                ' on line ' . $trace[0]['line'],
            E_USER_NOTICE
        );
        return null;
    }

    public function addParam($param, $data){
        $this->confs[$param] = $data; 
    }

    public function modifyOption($attr, $value)
    {
        $this->options['http'][$attr] = $value;
    }

    public function getOptions($data){
        if($this->options['http']['header'] == 'Content-type: application/json'){
            $data = json_encode($data);
        }else{
            $data = http_build_query($data);
        }
        $this->options['http']['content'] = $data;
        return $this->options;
    }

    private function makeUrl()
    {
        $port = $this->port ? ':' . $this->port : '';
        $service = $this->service ? '/' . $this->service : '';
        return $this->server . $port . $service;
    }

    public function require($texto)
    {

        $data = $this->confs; 
        
        $data[$this->textField] = $texto;       
        
        
        $options = $this->getOptions($data);

        $context  = stream_context_create($options);
        $response = file_get_contents($this->makeUrl(), false, $context);

        // Processar a resposta
        if ($response !== false) {
            ds($response);
            // A requisição foi bem-sucedida, faça algo com a resposta
            return $response;
        } else {
            // A requisição falhou, trate o erro
            return false;
        }
    }
}
