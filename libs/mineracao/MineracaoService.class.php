<?php

/**
 * Classe responsável por minerar textos e salvar 
 * 
 * Sobek (passa n maneiras)
 * Freeling
 * Yake
 * Cohmetrix
 * LanguageTool
 */
class MineracaoService{

    //O v1 no final determina a versão das minerações
    private $_DIR = CACHE . '/texts/v1_';

    //variaveis uteis
    private $text;
    private $cleanText;
    private $md5;
    private $dir;

    private $mineradores = [];

    //Passa um texto
    //Limpa texto (retorna o texto limpo)
    //Gerou a pasta 
    //Gera um pool de minerações  (Salva em uma tabela quais precisam ser feitos)

    public function __construct($texto)
    {
        $this->text = $texto;
       
        $this->salvaInicial();
        $this->local();

        $redacao = new Redacao();
        $redacao->titulo = "batta";
    }

    private function local()
    {
        $sobek = new MinerModel();
        $sobek->server = 'http://sobek';
        $sobek->service = 'bigSobek.php';
        $sobek->file = 'text.sobekv1';
        $sobek->textField = 'data';
        $sobek->addParam('args', '{"lang":"pt-br","return":"JSON", "nTerms": 10, "minTerms":4}');
        $this->mineradores['sobek'] = $sobek;

        $freeling = new MinerModel();
        $freeling->server = 'http://freeling';
        $freeling->file = 'text.freeling';
        $this->mineradores['freeling'] = $freeling;

        $languageTool = new MinerModel();
        $languageTool->server = 'http://languageTool';
        $languageTool->port = 8010;
        $languageTool->file = 'text.languageTool';
        $languageTool->addParam('language', 'pt-br');
        $languageTool->service= 'v2/check';
        $this->mineradores['languageTool'] = $languageTool;

        $yake = new MinerModel();
        $yake->server = 'http://yake';
        $yake->file = 'text.yake';
        $yake->port = 5000;       
        $yake->service = 'yake/';
        $yake->modifyOption('header', 'Content-type: application/json');
        $yake->addParam('language', 'pt');
        $yake->addParam('number_of_keywords', 15);
        $yake->addParam('max_ngram_size', 3);
        $this->mineradores['yake'] = $yake;

        $nilcmetrix = new MinerModel();
        $nilcmetrix->server = 'http://nilcmetrix';
        $nilcmetrix->file = 'text.nilcmetrix';
        $this->mineradores['nilcmetrix'] = $nilcmetrix;


    }

    private function limpaTexto($texto){
        #todo Fazer
        return $texto;
    }

    public function salvaInicial(){
        $texto =  $this->cleanText = $this->limpaTexto($this->text);

        $md5 = $this->md5 = md5($texto);
        $this->dir = $this->_DIR . $md5 . '/';

        if(!file_exists($this->dir)){
            mkdir($this->dir);
        }else{
            file_put_contents( $this->dir . 'text.raw', $this->text);
            file_put_contents( $this->dir . 'text.clean', $texto);
        }        

    }


    public function getRaw()
    {
        if(file_exists($this->dir . 'text.raw')){
            return file_get_contents($this->dir . 'text.raw');
        }else{
            $this->salvaInicial();
            return file_get_contents($this->dir . 'text.raw');
        }
    }

    public function getCleanText()
    {
        if(file_exists($this->dir . 'text.raw')){
            return file_get_contents($this->dir . 'text.raw');
        }else{
            $this->cleanText = $this->limpaTexto($this->text);
            file_put_contents( $this->dir . 'text.clean',  $this->cleanText );

            return $this->cleanText;
        }
    }

    /**
     * Verifica se já foi executado o minerador para o texto corrente.
     *
     * @param string $minerador nome do minerador
     * @return bool
     */
    public function verifica($minerador){
        if(isset($this->mineradores[$minerador])){
            $file = $this->mineradores[$minerador]->file;
            return file_exists($this->dir . $file);
        }
        return false;
    }

    public function getMineracao($minerador){
        if(isset($this->mineradores[$minerador])){
            return $this->cacheOuMinera($minerador);
        }else{
            //Lançar exceção
            return 'ERRO: Minerador indisponível';
        }
    }
    
    private function cacheOuMinera($minerador)
    {
        $file = $this->mineradores[$minerador]->file;
        if(file_exists($this->dir . $file)){
            return file_get_contents($this->dir . $file);
        }else{
            return $this->minera($minerador);
        }
    }

    private function minera($minerador)
    {
        $min = $this->mineradores[$minerador];

        $resultadoMineracao = $min->require($this->cleanText);
        if($resultadoMineracao){
            file_put_contents( $this->dir . $min->file,  $resultadoMineracao );
        }else{
            ds('ver de tratar o erro decentemente');
        }
    }

    


}

