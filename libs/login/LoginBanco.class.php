<?php

namespace core\libs\login;

use AbstractModel;

/**
 * Undocumented class
 */
class LoginBanco extends Login
{

    private $tabelaLogin;

    /**
     * Modelo de conexão ao banco de dados
     * @var \AbstractModel 
     */
    private $modelo;
    private $campoIdUsuario;
    private $camposAceitosLogin = array('email');
    private $camposAdicionais = array();

    /**
     * Construtor que determina qual vai ser a chave criptográfica
     * e qual vai ser a tabela para verificar login e senha
     * 
     * @param string $chave - Chave criptográfica para salgar algoritmo
     * @param string $tabelaLogin
     */
    public function __construct($chave, \AbstractModel $model, $tabelaLogin = 'usuario')
    {
        $this->tabelaLogin = $tabelaLogin;
        $this->chave = $chave;
        $this->modelo = $model;
        $this->campoIdUsuario = 'id_usuario';
    }

    /**
     * Recebe um objeto do tipo usuario para montar a query de forma 
     * adequada.
     *  
     * @param \DTOInterface $usuario
     */
    public function setUsuarioModel(\DTOInterface $usuario)
    {
        $this->tabelaLogin = $usuario->getTable();
    }

    /**
     * Retorna o valor da variável campoIdUsuario
     *
     * @return string - Valor da variável campoIdUsuario
     */
    public function getCampoIdUsuario()
    {
        return $this->campoIdUsuario;
    }

    /**
     * Método que seta o valor da variável campoIdUsuario
     *
     * @param string $campoIdUsuario - Valor da variável campoIdUsuario
     */
    public function setCampoIdUsuario($campoIdUsuario)
    {
        $campoIdUsuario = trim($campoIdUsuario);
        $this->campoIdUsuario = $campoIdUsuario;
        return true;
    }

    /**
     * 
     * @return string nome de usuário no banco de dados
     */
    public function getUsuario()
    {
        return $this->userObject->getLogin();
    }

    public function getEmail()
    {
        return $this->email;
    }

    private function montaCondicao($login)
    {
        $login = strtolower($login); //remover especial chars
        $login = str_replace("'", '', $login);//Primeiro nível de injection (Migrar para prepared)
        $cond = '';
        foreach ($this->camposAceitosLogin as $campo) {
            $cond .= $campo . " ='" . $login . "' OR ";
        }
        return rtrim($cond, ' OR ');
    }

    public function verificaLoginSenha($login, $senha)
    {
        //$this->modelo->prepareMode();
        $consulta = $this->modelo->queryTable($this->tabelaLogin,
                'senha, ' . $this->campoIdUsuario . $this->getCamposAdicionais(),
                $this->montaCondicao($login));
        $resultado = $consulta->fetchAll(\PDO::FETCH_ASSOC);
        $resultQtd = count($resultado);

        if ($resultQtd == 1) {
            $senhaCriptografada = $resultado[0]['senha'];
            if ($this->verificaSenha($senha, $senhaCriptografada)) {
                $this->geraObjSessao($login, $senha);
                $this->userObject->setId($resultado[0][$this->campoIdUsuario]);
                $_SESSION['user'] = serialize($this->userObject);
                return true; //Autenticação OK
            }
        } else if ($resultQtd > 1) {
            throw new \SQLException("Query montada errada ou modelagem inconssistente", 'QUERY sup');
        }
        return false;
    }

    public function getNivelUsuario()
    {
        return $this->nivel;
    }

    /**
     * Método genérico que recupera o usuario pelo seu id
     * 
     * @param string $usuario
     * @param \AbstractModel $modelo
     */
    public function recuperaUsuario($usuario, \AbstractModel $modelo)
    {
        $consulta = $modelo->queryTable($this->tabelaLogin, 'usuario',
                'id_usuario = ' . $this->idUsuario);
    }

    public function setModelo(AbstractModel $modelo)
    {
        $this->modelo = $modelo;
    }

     private function getCamposAdicionais()
    {
        if (sizeof($this->camposAdicionais)) {
            return ', ' . implode(',', $this->camposAdicionais);
        }
    }

}
