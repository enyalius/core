<?php

namespace core\libs\login;

/**
 * Classe responsável por ser um DTO padronizado para manter o enyalius 
 * logado em todas as páginas
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 2.0.0
 */
##[AllowDynamicProperties]
class User implements \JsonSerializable
{

    private $idUser;
    private $login = null;
    private $pass = null;
    private $groups = [];

    /**
     * Pack no padrão chave=>valor para adicionar propriedades no objeto
     *  de login
     * 
     * @var array 
     */
    private $extras = [];
    private $autenticador = 'LoginBD';

    /**
     * @deprecated since 2.0.0
     * @var int 
     */
    private $access = 1000;
    private $aes;

    public function __construct($login)
    {
        $this->login = $login;
        $this->aes = new \Aes();
    }

    /**
     * Criptografa a senha
     * 
     * #TODO ver de mudar para password_hash
     * 
     * @param string $senha
     * @return string
     */
    public static function geraHashSenha($senha)
    {
        return sha1(md5(LOGIN_CHAVE . sha1($senha) . LOGIN_CHAVE));
    }
    
    /**
     * 
     * @return User
     */
    public static function unserialize(){
        if (!isset($_SESSION['user'])){
            return null;
        }
        return unserialize($_SESSION['user']);
    }
    
    public static function ID(){
        $user = User::unserialize();
        if($user){
            return $user->getId();
        }
        return -1;
    }

    public function serialize()
    {
        $_SESSION['user'] = serialize($this);
    }

    public function addExtra($key, $value)
    {
        $this->extras[$key] = $value;
        $this->serialize();
    }

    public function getExtra($key, $default = null)
    {
        if (!isset($this->extras[$key])) {
            if(!is_null($default)){
                $this->addExtra($key, $default);
            }
            return '';
        }
        return $this->extras[$key];
    }

    public function getAutenticador()
    {
        return $this->autenticador;
    }

    public function setAutenticador($autenticador)
    {
        $this->autenticador = $autenticador;
        return $this;
    }

    public function getId()
    {
        return $this->idUser;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getPass()
    {
        if (empty($this->aes)) {
            $this->aes = new \Aes();
        }
        return trim($this->aes->decrypt($this->pass));
    }

    public function getAccess()
    {
        return $this->access;
    }

    public function setId($id)
    {
        $this->idUser = $id;
        return $this;
    }

    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    public function setPass($pass)
    {
        $this->pass = $this->aes->crypt($pass);
        return $this;
    }

    public function setAccess($access)
    {
        $this->access = $access;
        return $this;
    }

    public function __sleep()
    {
        return['idUser',
         'login', 'access',
         'extras' ,
         'pass',  
         'autenticador'];
    }

    public function jsonSerialize():mixed
    {
        return ['id' => $this->idUser,
            'login' => $this->login,
            'autenticador' => $this->autenticador,
            'data' => json_encode($this->extras)
        ];
    }

}
