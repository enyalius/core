<?php

trait SimpleCache{
    private $_cacheDir = CACHE;

    private function setCacheDir($cache){
        $this->_cacheDir = $cache;
    }
    
    private function iniciaCache(){
        if(!file_exists($this->_cacheDir)){
            mkdir($this->_cacheDir, 0755, true);
        }
    }

    private function cacheVerify($key){
        $this->iniciaCache();
        $key = md5($key);
        $file = $this->_cacheDir . '/' . $key;
        if(file_exists($file)){
            return file_get_contents($file);
        }
        return false;
    }

    private function makeCache($key, $value){
        $this->iniciaCache();

        $md5Key = md5($key);
        file_put_contents($this->_cacheDir . '/' .$md5Key, $value);
        file_put_contents($this->_cacheDir . '/' .$md5Key . '_key', $key);

    }
}