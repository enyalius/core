<?php

use core\libs\log\EnyLOG;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class _LOG
{

    private static $logger = null;

    private static function iniciaLogger()
    {
        if (self::$logger == null) {
            self::$logger = new EnyLOG();
        }
    }
    
    /**
     * Log Message in file and send mail
     * 
     * @param mixed $message
     * @return void
     */
    public static function fatalError($message){
        self::iniciaLogger();
        self::$logger->error($message);
        MailUtil::send(MAIL_USER, MAIL_USER, '[fatal error]', $message);
    }

    public static function error($message, ...$extras)
    {
        self::iniciaLogger();
        self::$logger->error($message);
    }

    public static function warning($message, ...$extras)
    {
        self::iniciaLogger();
        self::$logger->warning($message);
    }

    public static function put($message, $file)
    {
        self::iniciaLogger();
        self::$logger->put($message, $file);
    }

}
