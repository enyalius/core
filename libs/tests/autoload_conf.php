<?php

//Caminho para o diretório raiz do projeto e busca do arquivo de configuração
require_once __DIR__. '/../../../../../confs/config.php';

require_once CORE . 'controller/errorTracker/controle_erros.php';

require_once CORE . 'autoload.php';
require_once CORE . '/libs/tests/CSVFileIterator.php';


define('TESTS_APP_DIR', ROOT . '../testes/');