<?php

abstract class Arquivo{
    protected $movido = false;
    protected $nome;

    /**
     * 1 - Salvo em diretorio
     * 2 - Salvo em Bytes
     * 3 - Salvo como large object
     * @var int 
     */
    protected $_system = 1;

    /**
     * Método que move o arquivo enviado para um determinado diretório.
     * 
     * @param string $diretorio - diretorio para onde será movido o arquivo
     * @param string $nome - nome do arquivo
     */
    public abstract function mover($diretorio, $nome = false);

    public function getNome(){
        return $this->nome;
    }

}