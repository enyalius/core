<?php

/**
 * Classe responsável por manipular os arquivos enviados via upload
 * 
 * TODO: Implementar as versões de OID e demais opções aqui nesse caso ele apenas retorna o endereço para onde foi movido
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 28/12/2023
 */
class ArquivoOnline extends Arquivo implements \core\model\io\ObjetoInsercao 
{

    private $url;
    private $data = null;
 

    public function __construct($url)
    {        
        $this->url = $url;
    }

    public function getDataFile(){
        if(is_null($this->data)){
            $this->data = file_get_contents($this->url);
        }
        return $this->data;
    }


    public function mover($diretorio, $nome = false)
    { 
        $nome = !$nome ? md5(uniqid()): $nome;
        $this->nome = $nome;
        $this->movido = $diretorio . '/' .$nome;
        file_put_contents($this->movido, $this->getDataFile());
        return $this->movido;
    }

    /**
     * 
     * @return string
     */
    public function getArquivo()
    {
        if($this->movido){
            return $this->movido;
        }
        return $this->data;
    }

    public function codigoInsercao()
    {
              
        return $this->movido;
        
    }

}
