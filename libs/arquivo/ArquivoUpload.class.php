<?php

/**
 * Classe responsável por manipular os arquivos enviados via upload
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0 - 22/05/2011
 */
class ArquivoUpload extends Arquivo implements \core\model\io\ObjetoInsercao
{

    private $endServidor;
    private $erro;
    private $tamanho;
    private $tipo;
    private $oid;
    private $id = false;
    private $extensao = false;

    public function __construct($arquivo)
    {
        
        if (isset($arquivo['name'])) {
            $this->processFile($arquivo);
        } else if(is_string($arquivo) && isset($_FILES[$arquivo])){
            $this->processFile($_FILES[$arquivo]);
        }else {
            //Lançar exceção??
            $this->erro = 4;
        }
    }

    /**
     * Função que mapeia os campos para o objeto
     *
     * @param [type] $arquivo
     * @return void
     */
    private function processFile($arquivo)
    {

        $this->endServidor = $arquivo['tmp_name'];
        $this->erro = $arquivo['error'];
        $this->tamanho = $arquivo['size'];
        $this->tipo = $arquivo['type'];
        $this->nome = $arquivo['name'];
    }

    /**
     * Verifica se o arquivo foi enviado corretamente
     * 
     * @return boolean 
     */
    public function isValid()
    {
        if ($this->erro == UPLOAD_ERR_OK) {
            return true;
        } else {
            return false;
        }
    }

    public function getErrorMessage()
    {
        if ($this->erro == UPLOAD_ERR_OK) {
            return 'Não houve erro ao carregar arquivo';
        } else if ($this->erro == UPLOAD_ERR_NO_FILE) {
            return 'Você deve enviar ao menos um arquivo';
        } else if ($this->erro == UPLOAD_ERR_INI_SIZE) {
            return 'O arquivo enviado é muito grande!';
        }  else  {
            #FIXME criar outras mensagens de erro
            return 'Erro ao carregar arquivo';
        }
    }

    /**
     * 
     * @return string
     */
    public function getArquivo()
    {
        if($this->movido){
            return $this->movido;
        }
        return $this->endServidor;
    }

    /**
     * Retorna o tamanho do arquivo.
     *
     * @return integer - Tamanho do arquivo
     */
    public function getTamanho()
    {
        return $this->tamanho;
    }

    /**
     * Retorna o mime/type do arquivo
     * 
     * @return string Mime do arquivo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getOid()
    {
        return $this->oid;
    }

    public function setOid($oid)
    {
        $this->oid = $oid;
    }

    /**
     * Método que move o arquivo enviado para um determinado diretório.
     * 
     * @param string $diretorio - diretorio para onde será movido o arquivo
     * @param string $nome - nome do arquivo
     */
    public function mover($diretorio, $nome = false)
    {
        if(!$this->isValid()){
            throw new ErrorException("Arquivo com problemas: " . $this->getErrorMessage());
        }
        $nome = !$nome ? $this->nomePorId() : $nome;
        $this->nome = $nome;
        $movido = move_uploaded_file($this->endServidor, $diretorio . '/' . $nome);
        if($movido){
            $this->movido = $diretorio . '/' . $nome;
        }
        return $movido;
    }

    public function __toString()
    {
        return $this->nome . ' ' . $this->tipo;
    }

    public function codigoInsercao()
    {
        if ($this->_system == 3) {
            return $this->oid;
        } elseif ($this->_system == 2) {
            $tamanhoImg = filesize($this->endServidor); 
            $file = addslashes(fread(fopen($this->endServidor, "r"), $tamanhoImg));  
            return $file;
        } else {
            return $this->getNome();
        }
    }

    public function getBDType()
    {
        if ($this->_system == 3) {
            return 'lo';
        } elseif ($this->_system == 2) {
            return 'bytea';
        } else {
            return 'varchar';
        }
    }

    public function nomePorValor($nome = false)
    {
        $nomeTmp = $nome == false ? $this->nome : $nome;
        $nomeFinal = StringUtil::slugify($nomeTmp);

        return $nomeFinal . '.' . $this->getExtensao();
    }

    /**
     * Gera um nome para o arquivo, caso o mesmo possua um id retorna 
     * o nome do arquivo com um id, caso contrário gera u nome único para salvar 
     * em uma pasta
     * 
     * @return string nomeUnico.ext
     */
    public function nomePorId()
    {
        if ($this->id === false) {
            $nome = md5(uniqid());
        } else {
            $nome = $this->id;
        }
        return $nome . '.' . $this->getExtensao();
    }

    public function getExtensao()
    {
        if (!$this->extensao) {
            $pieces = explode('.', $this->nome);
            $this->extensao = end($pieces);
        }
        return $this->extensao;
    }

    public function setDataBD($data)
    {
        if ($this->_system == 3) {
            $this->oid = $data;
        }
    }

}
