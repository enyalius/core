<?php

/**
 * A implementação da classe AES é baseada na documentação oficial disponibilizada em 
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 2.0.0
 * 
 */
class Aes
{

    private $key;
    private $crypto;
   
    public function __construct($key = LOGIN_CHAVE)
    {
        $this->key = hash('md5', $key);
        $this->crypto = defined('CRYPTO') ? CRYPTO : "AES-256-CBC"; 
    }
    
 
    /**
     * Undocumented function
     *
     * @deprecated version
     * @param [type] $valor
     * @return void
     */
    public function crypt($valor)
    {
        return $this->encrypt($valor);
    }

    /**
     * Criptografa um dados utilizando o algoritmo AES 256 CBC
     * 
     * @param string $valor
     * @return string base64 Dado e IV criptografado
     */
    public function encrypt($valor){
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($valor, $this->crypto, $this->key, 0, $iv);
        return base64_encode($encrypted . '::' . $iv);
    }

    public function decrypt($cript)
    {
        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($eData, $iv) = explode('::', base64_decode($cript), 2);
        $decript = openssl_decrypt($eData, $this->crypto, $this->key, 0, $iv);
        return $decript; 
    }

}
