<?php

/**
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
class BDHelper
{

    /**
     *
     * @var PDO 
     */
    protected $database;
    private $debug;
    private $lastQuery = '';
    protected static $inTransaction = false;
    private static $strConexao = '';
    private static $conexao = null;
    private $error = [];
    private static $debugGlobal = false; //Se ativar todas as quer
    private $prepareMode = false;

    /**
     * 
     * @param string $dbName
     * @param string $databaseType
     * @param string $dbServer
     * @param string $user
     * @param string $password
     */
    public function __construct($dbName, $databaseType, $dbServer, $user, $password)
    {
        try {
            $str = $databaseType . ':host=' . $dbServer . ';dbname=' . $dbName;
            $this->database = $this->verificaConexao($str, $user, $password);
            $this->debugOn(self::$debugGlobal);
            $this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $ex) {
            echo 'Estamos com problemas o sistema pode não funcionar corretamente.';
            _LOG::error('Falha na conexão' . $ex->getMessage());
            exit();
        }
    }

    public function pdo(){
        return $this->database;
    }

    public static function debugALL($status = true)
    {
        self::$debugGlobal = $status;
    }

    private function verificaConexao($str, $user, $password)
    {
        if (self::$conexao != null && self::$strConexao == $str . $user . $password) {
            return self::$conexao;
        } else {

            self::$strConexao = $str . $user . $password;
            self::$conexao = new PDO($str, $user, $password);
            return self::$conexao;
        }
    }

    public function debugOn($status = true)
    {
        $this->debug = $status;
    }

    public function begin()
    {
        if (!$this->inTransaction()) {
            self::$inTransaction = $this->database->beginTransaction();
        }
        return self::$inTransaction;
    }

    public function rollback()
    {       
        if ($this->inTransaction()) {
            $this->database->rollBack();
        }
        self::$inTransaction= false;
        return true;
    }

    /**
     * Realiza o commit na transação corrente e libera o recurso para outra transação.
     *
     * @return boolean
     */
    public function commit()
    {
        if ($this->inTransaction()) {
            self::$inTransaction= false;
            return $this->database->commit();
        }
        return false;
    }

    public function inTransaction()
    {
        return self::$inTransaction;
    }

    public function saveFile($table, $colunm, $file, $extras = array())
    {
        throw new ProgramacaoException('Código não implementado para o SGBD em uso');
    }

    /**
     * Método que prepara o LargeObject ou o array de bytes para salvar em coluna
     * 
     * @param Arquivo $file
     * @param string [lo] Tipo do salvamento em coluna byte ou largeObject - Padrão lo
     * @return mixed
     */
    public function saveFileInColunm($file, $type = 'lo')
    {
        throw new ProgramacaoException('Código não implementado para o SGBD em uso');
    }

    /**
     * Método que le um arquivo do banco de dados
     * 
     * @param string $table - nome da tabela
     * @param string $colunm
     * @param int $id - seletor
     * @param array $extras - Opções extras que podem ser usados 
     * @throws ProgramacaoException
     */
    public function readFile($table, $colunm, $file, $extras = [])
    {
        throw new ProgramacaoException('Código não implementado para o SGBD em uso');
    }

    private function verificaArquivo($arquivo)
    {
        if (get_class($arquivo) == 'ArquivoUpload') {
            $data = $this->saveFileInColunm($arquivo->getArquivo(), $arquivo->getBDType());
            $arquivo->setDataBD($data);
        }
    }

    public function queryAsArray($sql)
    {
        $return = [];
        $data = [];

        foreach ($this->database->query($sql) as $return) {
            array_push($data, $return);
        }

        return $data;
    }

    /**
     * 
     * @param string $sql
     * @return PDOStatement
     * @throws SQLException
     */
    public function query($sql)
    {
        $this->lastQuery = $sql;
        if ($this->debug || self::$debugGlobal) {
            DebugUtil::showCode($sql);
        }
        try {
            if($this->prepareMode){
                return $this->database->prepare($sql);
            }
            return $this->database->query($sql);
        } catch (PDOException $exception) {
            throw new SQLException($exception, $sql);
        }
    }

    /**
     * Método que monta uma query SQL 
     * 
     * @param mixed $table
     * @param mixed [$fields]
     * @param Prepared|string [$condition] Se String aplica a condição caso, for um array usa Prepared
     * @param mixed [$order]
     * @param mixed [$limit]
     * @return PDOStatement
     */
    public function queryTable($table, $fields = '*', $condition = null, $order = null, $limit = null, $offset = null)
    {
        $sql = 'SELECT ' . $fields . ' FROM ' . $table;

        if (! empty($condition)) {
            $sql .= ' WHERE ' . $condition;
        }

        if (!empty($order)) {
            $sql .= ' ORDER BY ' . $order;
        }

        if (!empty($limit)) {
            $sql .= ' LIMIT ' . $limit;
        }

        if (!empty($offset)) {
            $sql .= ' OFFSET ' . $offset;
        }

        if (is_object($condition)) {
            return $this->executePreparedQuery($sql, $condition->getData());
        }
        return $this->query($sql);
    }

    /**
     * Summary of executePreparedQuery
     * 
     * @param mixed $sql
     * @param mixed $data
     * @throws \SQLException
     * @return PDOStatement|array|bool
     */
    public function executePreparedQuery($sql, $data){
        try {
            if ($this->debug || self::$debugGlobal) {
                DebugUtil::showCode($sql);
                DebugUtil::show($data);
            }
            $this->lastQuery = $sql;
            $stmt = $this->database->prepare($sql);
            $stmt->execute($data);
            return $stmt->fetchAll();
        } catch (PDOException $exception) {
            if ($this->debug || self::$debugGlobal) {
                ds($exception->getMessage());
                throw new SQLException($exception, $sql, $data);
            }
            $this->error[] = [$exception, $sql, $data];
        }
        return false;
    }

    private function geraStringDeInsercao(&$data, $key, $value)
    {
        $valor = '?';
        if (is_object($value)) {
            unset($data[$key]);
            $this->verificaArquivo($value);
            $valor = $value->codigoInsercao();
        }
        return $valor;
    }

    /**
     * Realiza um insert no banco de dados.
     * 
     * @param string $table
     * @param mixed $data
     * @return boolean
     */
    public function insert($table, $data)
    {
        $sql = 'INSERT INTO ' . $table . ' ';

        //Verifica se é array para percorrer com foreach
        if (is_array($data)) {
            $sql .= '(';
            $values = '';
            //Acrescenta o nome dos campos e valida objetos
            foreach ($data as $name => $value) {
                $sql .= $name . ',';
                $values .= $this->geraStringDeInsercao($data, $name, $value) . ',';
            }

            //Retira a última vírgula, para não ser necessário fazer um contador
            $sql = trim($sql, ',');
            $sql .= ') ';
            $sql .= 'VALUES (' . trim($values, ',') . ')';
            return $this->execute($sql, $data);
        } else {
            $sql .= $data;
        }

        return $this->exec($sql);
    }

    public function update($table, $data, $condition)
    {
        $sql = 'UPDATE ' . $table . ' SET ';
        $isArray = false;
        //Verifica se é objeto ou array para percorrer com foreach
        if (is_array($data)) {
            //Percorre os campos e os valores
            foreach ($data as $name => $value) {
                $valor = $this->geraStringDeInsercao($data, $name, $value);
                $sql .= $name . '= ' . $valor . ' ,';
            }
            $sql = trim($sql, ','); //Retira a vírgula excedente
            $isArray = true;
        } else {
            $sql .= $data;
        }
        if (isset($condition)) {
            $sql .= ' WHERE ' . $condition;
        }
        if ($isArray) {
            return $this->execute($sql, $data);
        }

        return $this->exec($sql);
    }

    public function getLastQuery()
    {
        return $this->lastQuery;
    }

    public function delete($table, $condition)
    {
        if (is_array($condition)) {
            $condicao = '';
            foreach ($condition as $key => $value) {
                $condicao .= $key . ' = ' . $value;
            }
            $condition = $condicao;
        }

        $sql = 'DELETE FROM ' . $table . ' WHERE ' . $condition;
        return $this->exec($sql);
    }

    /**
     * Método que executa um prepared statment no banco
     * 
     * @param string $sql
     * @param array $data
     * @return bool <b>TRUE</b> no caso de sucesso ou <b>FALSE</b> caso ocora alguma falha.
     * @throws SQLException
     */
    private function execute($sql, $data)
    {
        try {
            if ($this->debug || self::$debugGlobal) {
                DebugUtil::showCode($sql);
                DebugUtil::show($data);
            }
            $this->lastQuery = $sql;
            $stmt = $this->database->prepare($sql);
            return $stmt->execute(array_values($data));
        } catch (PDOException $exception) {
            if ($this->debug || self::$debugGlobal) {
                ds($exception->getMessage());
                throw new SQLException($exception, $sql, array_values($data));
            }
            $this->error[] = [$exception, $sql, array_values($data)];
            return false;
        }
    }

    /**
     * Função que executa uma string de SQL no banco de dados
     * 
     * @param string $sql
     * @return int|bool numeros de linhas e FALSE em caso de falha
     * @throws SQLException
     */
    public function exec($sql)
    {
        try {
            if ($this->debug || self::$debugGlobal) {
                DebugUtil::showCode($sql);
            }
            $this->lastQuery = $sql;
            return $this->database->exec($sql);
        } catch (PDOException $exception) {
            if ($this->debug || self::$debugGlobal) {
                throw new SQLException($exception, $sql);
            }
            $this->error[] = [$exception, $sql];
            return false;
        }
    }

    /**
     * 
     * @param string $name sequence_name ou tabela
     * @return int
     */
    public function lastInsertId($name)
    {
        return $this->database->lastInsertId($name);
    }

    public function getLogErrors()
    {
        return $this->error;
    }

    /**
     * 
     */
    public function __destruct()
    {
        if ($this->inTransaction()) {
            $this->commit(); //FIXME ver se não é melhor lançar uma exceção
        }
        $this->database = null;
    }
}
