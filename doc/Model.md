##Model

O model no Enyalius normalmente é composto por 2 classes a Classe [DTO](doc/model/dto.md) e a classe [DAO](doc/model/dto.md).

Usuario.class.php
```
**
 * Classe para a transferencia de dados de Usuario entre as 
 * camadas do sistema 
 *
 * @package app.model.dto
 * @author  Marcio Bigolin <marcio.bigolinn@gmail.com> 
 * @version 1.0.0 - 11-06-2017(Gerado Automaticamente com [GC] - 1.1 11/07/2017)
 */
class Usuario implements DTOInterface
{

    use core\model\DTOTrait;

    private $idUsuario;
    private $email;
    private $nome;
    private $senha;
    private $foto;
    private $sexo = 0;
    private $dataNascimento;
    private $isValid;
    private $papeis = array();
    private $table;

    /**
     * Método Construtor da classe responsável por setar a tabela 
     * e inicializar outras variáveis
     *
     * @param string $table -  Nome da tabela no banco de dados
     */
    public function __construct($table = 'public.usuario')
    {
        $this->table = $table;
    }

    public function isAdmin()
    {
        return in_array('admin', $this->papeis);
    }

    public function addPapel($papel)
    {
        $this->papeis[] = $papel;
    }

    /**
     * Retorna o valor da variável idUsuario
     *
     * @return Inteiro - Valor da variável idUsuario
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Método que seta o valor da variável idUsuario
     *
     * @param Inteiro $idUsuario - Valor da variável idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
        if (empty($idUsuario)) {
            $GLOBALS['ERROS'][] = 'O valor informado em Id usuário não pode ser nulo!';
            return false;
        }
        if (!(is_numeric($idUsuario) && is_int($idUsuario + 0))) {
            $GLOBALS['ERROS'][] = 'O valor informado em Id usuário é um número inteiro inválido!';
            return false;
        }
        $this->idUsuario = $idUsuario;
        return true;
    }

    /**
     * Retorna o valor da variável email
     *
     * @return string - Valor da variável email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Método que seta o valor da variável email
     *
     * @param string $email - Valor da variável email
     */
    public function setEmail($email)
    {
        if (empty($email)) {
            $GLOBALS['ERROS'][] = 'O valor informado em Email não pode ser nulo!';
            return false;
        }
        $this->email = $email;
        return true;
    }

    /**
     * Retorna o valor da variável nome
     *
     * @return string - Valor da variável nome
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Método que seta o valor da variável nome
     *
     * @param string $nome - Valor da variável nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return true;
    }

    /**
     * Retorna o valor da variável senha
     *
     * @return string - Valor da variável senha
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * Método que seta o valor da variável senha
     *
     * @param string $senha - Valor da variável senha
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
        return true;
    }

    /**
     * Retorna o valor da variável foto
     *
     * @return string - Valor da variável foto
     */
    public function getFoto()
    {
        return $this->foto;
    }
    
    public function mostrarFoto(){
        if(empty($this->getFoto())){
            //Try Gravatar
            return AvatarUtil::gravatar($this->email, BASE_URL . '/imagens/no_avatar.jpg');
        }else{
            return '/perfil/verFoto';
        }
    }

    /**
     * Método que seta o valor da variável foto
     *
     * @param string $foto - Valor da variável foto
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
        return true;
    }

    /**
     * Retorna o valor da variável sexo
     *
     * @return Inteiro - Valor da variável sexo
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Método que seta o valor da variável sexo
     *
     * @param Inteiro $sexo - Valor da variável sexo
     */
    public function setSexo($sexo)
    {  
        if (!(is_numeric($sexo) && is_int($sexo + 0))) {
            $GLOBALS['ERROS'][] = 'O valor informado em Sexo é um número inteiro inválido!';
            return false;
        }
        $this->sexo = $sexo;
        return true;
    }

    /**
     * Retorna o valor da variável dataNascimento
     *
     * @return string - Valor da variável dataNascimento
     */
    public function getDataNascimento()
    {
        return $this->dataNascimento;
    }

    /**
     * Retorna o valor da variável dataNascimento formatada 
     *
     * @return string - Valor da variável dataNascimento formatada 
     */
    public function getDataNascimentoFormatada($extenco = true)
    {
        return DateUtil::formataData($this->dataNascimento);
    }

    /**
     * Método que seta o valor da variável dataNascimento
     *
     * @param string $dataNascimento - Valor da variável dataNascimento
     */
    public function setDataNascimento($dataNascimento)
    {
        $this->dataNascimento = DateUtil::preparaData($dataNascimento);
        return true;
    }

    /**
     * Retorna o valor da variável $tabela 
     *
     * @return string - Tabela do SGBD
     */
    public function getTable()
    {
        return $this->table;
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * Método responsável por retornar um array em formato JSON 
     * para poder ser utilizado como Objeto Java Script
     *
     * @return Array -  Array JSON
     */
    public function getArrayJSON()
    {
        return [
            '<a href="/admin/usuario/modoOdin?idUser=' . $this->idUsuario . '">' .$this->idUsuario .'</a>',
            $this->email,
            $this->nome,
            $this->senha,
            $this->foto,
            $this->sexo,
            $this->dataNascimento
        ];
    }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return string - Condição para selecionar um dado unico na tabela
     */
    public function getID()
    {
        return $this->idUsuario;
    }

    /**
     * Método utilizado como condição de seleção de chave primária
     *
     * @return string - Condição para selecionar um dado unico na tabela
     */
    public function getCondition()
    {
        return 'id_usuario = ' . $this->idUsuario;
    }

    public function __toString(){
        return $this->nome;
    }
```

