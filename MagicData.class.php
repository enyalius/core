<?php 
/**
 * Trait com gets e sets mágicos para os DTOs
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
trait MagicData{
    private $_notSetAlowed = [];

    /**
     * Método mágico get que permite acesso direto a uma propriedade do objeto 
     * 
     * OBS: São super protegidas propriedades que começarem pelo underscore $_propriedade
     * 
     * OBS2: Caso tenha um método getPropriedade() a mesma será usada para acesso a propriedade
     *
     * @param string $name
     * @return void
     */
    public function __get($name)
    {
        //Se existir getObjeto
        $method = 'get' . ucfirst($name);
        if (method_exists($this, $method)) {
            return $this->{$method}();
        }

        if (isset($this->{$name}) && $name[0] != '_') {
            return $this->{$name};
        }
        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
                ' in ' . $trace[0]['file'] .
                ' on line ' . $trace[0]['line'],
            E_USER_WARNING
        );
        return null;
    }

    /**
     * Set mágico que permite acesso direto a uma propriedade do objeto 
     * 
     * OBS: São super protegidas propriedades que começarem pelo underscore $_propriedade
     * 
     * OBS2: Caso tenha um método setPropriedade() a mesma será usada para acesso a propriedade
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $realPrivate = $this->_notSetAlowed;
        if (!in_array($name, $realPrivate) &&  $name[0] != '_') {
            $metodoName = 'set' . ucfirst($name);
            if (method_exists($this, $metodoName)) {
                return $this->{$metodoName}($value);
            }
            $this->{$name} = $value;
            return $this;
        }
        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
                ' in ' . $trace[0]['file'] .
                ' on line ' . $trace[0]['line'],
            E_USER_WARNING
        );
    }

    /**
     * Método que permite usar get e set mágico mesmo 
     * usando sintaxe de get e set do Javinha
     *
     * @param string $metodo
     * @param mixed $args
     * @return void
     */
    public function __call($metodo, $args)
    {
        if (strpos($metodo, 'set') === 0) {
            $prop = lcfirst(str_replace('set', '', $metodo));
            if (property_exists($this, $prop) && $prop[0] != '_') {
                $this->{$prop} = $args[0];
                return $this;
            }
        }
        if (strpos($metodo, 'get') === 0) {
            $prop = lcfirst(str_replace('get', '', $metodo));
            if (property_exists($this, $prop) && $prop[0] != '_') {
                return $this->{$prop};
            }
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined method via __call(): ' . $metodo .
                ' in ' . $trace[0]['file'] .
                ' on line ' . $trace[0]['line'],
            E_USER_WARNING
        );
    }
}